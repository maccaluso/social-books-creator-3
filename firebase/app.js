import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/messaging'

import env from 'config'

const config = {
  apiKey: env.FIREBASE.API_KEY,
  authDomain: `${env.FIREBASE.PROJECT_ID}.firebaseapp.com`,
  databaseURL: `https://${env.FIREBASE.DATABASE_NAME}.firebaseio.com`,
  projectId: env.FIREBASE.PROJECT_ID,
  storageBucket: `${env.FIREBASE.PROJECT_ID}.appspot.com`,
  messagingSenderId: env.FIREBASE.SENDER_ID,
  appId: env.FIREBASE.CONFIG_WEB_APP_ID
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
} else {
  firebase.app()
}
if (process.browser && firebase.messaging.isSupported()) {
  firebase.messaging = firebase.messaging()

  firebase.messaging.usePublicVapidKey(
    'BB1iS_3kd47LlnDoQXiV2dF0QajqXbEDO4GHn0UmCQQIz3X3sTSP6eroA1FG7QCMA1Ouo2QAgb3c3r-ZUdUhcAM'
  )
}

export default firebase
