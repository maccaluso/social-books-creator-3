export default {
  shared: {
    login: 'Log in',
    logout: 'Log out',
    register: 'Register',
    resetPswd: 'Reset password',
    forgotPswd: 'Forgot your password?',
    or: 'or',
    welcome: 'Welcome',
    homePage: 'Home page',
    home: 'Home',
    save: 'Save',
    reset: 'Reset',
    upload: 'Upload',
    remove: 'Remove',
    back: 'Back',
    forward: 'Forward',
    prev: 'Prev',
    next: 'Next',
    cancel: 'Cancel',
    delete: 'Delete',
    change: 'Change',
    savingFail: 'Saving failed',
    oops: 'Ooops, something went wrong',
    greatSaved: 'Great! Content saved',
    accept: 'Accept',
    deny: 'Deny',
    confirm: 'Confirm',
    formLabels: {
      name: 'Name',
      surname: 'Surname',
      email: 'Email',
      address: 'Address',
      bio: 'Biography',
      password: 'Password',
      passwordRepeat: 'Repeat password',
      newPassword: 'New password',
      groupName: 'Group name',
      groupInvited: 'Invited users',
      groupMembers: 'Group members',
      groupSharedBooks: 'Shared books',
      organization: 'Your organization or school'
    },
    toastMessages: {
      newGroupCreated: 'Great! Your new group is ready to kick off.'
    }
  },
  publicFrontend: {
    baseTitle: 'Welcome'
  },
  errorPage: {
    fourZeroFour: '404 Not Found',
    genericError: 'An error occurred'
  },
  components: {
    flashes: {
      verifyEmail: 'Please, take a minute to verify your email address.',
      verifyEmailBtnLabel: 'Re-send verification email',
      verifyEmailSuccess: 'An email has been sent to your address. Click the link provided in the message to verify your email.',
      pendingInvitations: 'You have pending invitations',
      pendingSync: 'Book Sync pending',
      goBookShelf: 'Go to the book shelf'
    },
    userSidebarWidget: {
      navItems: {
        dashboard: 'Dashboard',
        personalInfo: 'Personal info',
        groups: 'Groups',
        comments: 'Comments'
      },
      adminNavItems: {
        adminDashboard: 'Admin dashboard',
        users: 'Users',
        books: 'Books'
      }
    },
    mainNav: {
      bookShelf: 'Book shelf'
    },
    userNavWidget: {
      howdy: 'Howdy guest!',
      awesomeExperience: 'Welcome to the awesome Social Books Creator experience.',
      toGetStarted: 'To get started, please',
      accessProfile: 'Access your profile and settings.',
      profile: 'Profile',
      editorSettings: 'Editor settings'
    },
    personalDataForm: {
      heading: 'Your personal info'
    },
    downloadPersonalData: {
      json: 'Download Json',
      info: 'Lorem ipsum dolor sit amet etc etc'
    },
    avatarWidget: {
      heading: 'Avatar settings'
    },
    loginForm: {
      heading: 'Login form',
      noAccount: 'Don\'t have an account?'
    },
    registerForm: {
      heading: 'Register form',
      alreadyAccount: 'Already have an account?'
    },
    resetPasswordForm: {
      heading: 'Reset password',
      pswdRemembered: 'Did you remember your password?'
    },
    confirmPasswordResetForm: {
      heading: 'Pick a new password',
      confirmPswd: 'Confirm your new password',
      nowCanLogin: 'Now you can login with your new password'
    },
    changePassword: {
      typeOldPwd: 'Type your old password',
      typeNewPwd: 'Type your new password',
      typeNewPwd2: 'Confirm your new password',
      pwdUpdated: 'Password updated!'
    },
    groupList: {
      heading: 'My groups',
      newGroup: 'New group'
    },
    newGroupModal: {
      pickName: 'Pick a name for your new group',
      inviteCollaborators: 'Invite other users to collaborate on this group',
      shareBooks: 'Share one or more of your books with this group',
      createNewGroup: 'Create new group',
      groupNameCaption: 'This will be the name of your group (e.g. "IVth yr")',
      groupInvitedCaption: 'Group members can view this group activity and edit shared books',
      groupSharedBooksCaption: 'These will be the books the group will work on. You can skip this step and create and share a new book later'
    },
    preferencesWidget: {
      title: 'Preferences',
      week: 'Every week',
      weeks2: 'Every two weeks',
      month: 'Every month',
      defaultLang: 'Select you default language',
      hints: 'Toggle hints',
      mailFreq: 'Set your mail frequency',
      mailFromUs: 'Receive mail from us'
    },
    privacyWidget: {
      title: 'Security & Privacy',
      changePwd: 'Change password',
      dwnldPersonalData: 'Download personal data'
    },
    notifications: {
      syncBookNotification: 'Your shared book {text} can be synchronized',
      groupInvitationReceived: 'You are invited to join the group "{text}"',
      userAcceptInvitation: 'The user {text} has accepted your invitation',
      requestModification: 'User {text} sent a change to a shared book',
      ownerAcceptedChanges: 'The administrator of the {text} group has accepted your changes',
      yourNotifications: 'Your notifications',
      noNotifications: 'You don\'t have notifications'
    },
    bookCard: {
      saveTemplate: 'Save as template',
      duplicate: 'Duplicate',
      downloadPdf: 'Download PDF',
      openEditor: 'Open in Editor'
    },
    invitationsWidget: {
      invitations: 'Invitations'
    },
    profileDataForm: {
      nameReq: 'Name is required',
      surnameReq: 'Surname is required',
      bioRules: 'Biography should be less than, or equal to, 300 characters'
    }
  },
  pages: {
    emailLanding: {
      verifyingAddress: 'Please, wait a minute, we\'re checking your email address.',
      emailVerified: 'Great! Your email has been verified.',
      emailVerificationeError: 'Ops! We weren\'t able to verify your email.'
    },
    bookShelf: {
      noSharedBooks: 'You have no shared books yet...',
      sharedBooks: 'Here you will find the books you\'re collaborating on.',
      learnMore: 'Learn more about Social Books Creator collaborative workflow...',
      sharedBooksTitle: 'Books shared with you',
      createNewBook: 'Create new book',
      noBooks: 'No books yet...',
      startCreating: 'Start by creating a new one.',
      addBook: 'Add Book',
      addBooks: 'Add Books',
      manageRequests: 'Manage Requests'
    }
  },

  app: {
    bookShelf: {
      yourBooks: 'Your books'
    },
    editor: {
      bookSettings: {
        sectionLabel: 'Book settings',
        title: 'Title',
        abstract: 'Abstract',
        saved: 'Book settings saved...',
        headingFont: 'Headings font',
        size: 'Size',
        bodyFont: 'Body font',
        pageSize: 'Page size',
        orientation: 'Page orientation',
        formatOptions: 'Formatting options',
        license: 'Edit license',
        unpublish: 'Unpublish',
        publish: 'Publish',
        publishingSettings: 'Publishing settings',
        bookInfo: 'Book info',
        bookCoverEditor: 'Cover editor'
      },
      chapters: {
        structureSaved: 'Chapter structure saved...',
        deleteSub: 'Delete subchapter',
        moveDown: 'Move down',
        moveUp: 'Move up',
        subSettings: 'Subchapter settings',
        delete: 'Delete chapter',
        settings: 'Chapter settings',
        addSub: 'Add subchapter',
        add: 'Add chapter'
      },
      layout: {
        placeholder: 'Let`s write an awesome story!',
        showBlocks: 'Show blocks',
        showColors: 'Show colors',
        hideBlocks: 'Hide blocks',
        hideColors: 'Hide colors'
      },
      preview: {
        expand: 'Expand the section to preview'
      }
    },
    shared: {
      deleteBook: 'Yes, delete this book',
      deleteSure: 'Are you sure you want to proceed?',
      deleteLost: 'All your work on this book will be lost!',
      pickTitle: 'Pick a title for your book',
      chooseCC: 'Choose a CC licence for your book',
      abstract: 'Provide an abstract for your book',
      createBook: 'Create your book',
      bookSummary: 'A brief summary or presentation text. Not mandatory. Can be skipped.',
      bookLicense: 'This is the license with which your book will be published.',
      license: 'License',
      bookTitle: 'This is the main title of your book (e.g. "Moby Dick")'
    }
  },

  group: {
    modsSumitted: 'Your changes were sumbitted to group owner!',
    submitChanges: 'Submit your changes',
    openSyncDialog: 'Open Sync Dialog',
    syncPending: 'Sync pending.',
    syncFailed: 'Sync failed',
    syncOk: 'Sync completed',
    syncPage: 'Sync Page',
    masterBookUpdated: 'Master copy of this book has been updated',
    alertBeforeSync: 'Please, be aware that by proceeding, work not yet integrated into the master copy will be lost. Consider submitting it before syncing.',
    completeSync: 'COMPLETE SYNC',
    partialSync: 'PARTIAL SYNC',
    unsubscribe: 'Unsubscribe from this group',
    reviewDiffs: 'Review differences',
    diffsMerged: 'Differences merged!',
    admin: 'Admin',
    members: 'Members',
    noMembers: 'There are no members',
    invited: 'Invited',
    addMember: 'Add Member',
    noInvitations: 'There aren\'t pending invitations',
    addBookToGroup: 'Add book to this group',
    userDeleted: 'User deleted!',
    userNotFound: 'User {user} not found!',
    usersInvited: 'Users invited',
    manageRequests: 'Manage Requests',
    inviteAccepted: 'You are now a member of the \'{group}\' group!',
    deleteGroup: 'Delete group',
    viewGroup: 'View group',
    editGroup: 'Edit group',
    viewChanges: 'View changes'
  },

  auth: {
    emailReq: 'Email is required',
    pwdReq: 'Password is required',
    emailInvalid: 'Invalid email address',
    pwd6char: 'Password should be at least 6 characters',
    newPwdSet: 'Your new password has been set...',
    repeatPwdReq: 'Repeat password is required',
    pwdMatch: 'Passwords must match',
    resetPwdEmailSent: 'Password reset email sent...',
    pwdUpdated: 'Password updated!'
  }
}
