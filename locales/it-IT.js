export default {
  shared: {
    login: 'Accedi',
    logout: 'Esci',
    register: 'Registrati',
    resetPswd: 'Reimposta password',
    forgotPswd: 'Hai dimenticato la password?',
    or: 'o',
    welcome: 'Benvenuto/a',
    homePage: 'Home page',
    home: 'Home',
    save: 'Salva',
    reset: 'Ripristina',
    upload: 'Carica',
    remove: 'Rimuovi',
    back: 'Indietro',
    forward: 'Avanti',
    prev: 'Prec.',
    next: 'Succ.',
    cancel: 'Annulla',
    delete: 'Cancella',
    change: 'Modifica',
    savingFail: 'Impossibile salvare',
    oops: 'Ooops, qualcosa è andato storto',
    greatSaved: 'Ottimo! Contenuti salvati',
    accept: 'Accetta',
    deny: 'Rifiuta',
    confirm: 'Conferma',
    formLabels: {
      name: 'Nome',
      surname: 'Cognome',
      email: 'Email',
      address: 'Indirizzo',
      bio: 'Biografia',
      password: 'Password',
      passwordRepeat: 'Ripeti password',
      newPassword: 'Nuova password',
      groupName: 'Nome gruppo',
      groupInvited: 'Utenti invitati',
      groupMembers: 'Membri del gruppo',
      groupSharedBooks: 'Libri condivisi',
      organization: 'La tua scuola o organizzazione'
    },
    toastMessages: {
      newGroupCreated: 'Fantastico! Il tuo nuovo gruppo è pronto per partire.'
    }
  },
  publicFrontend: {
    baseTitle: 'Benvenuti'
  },
  errorPage: {
    fourZeroFour: '404 Pagina non trovata',
    genericError: 'C\'è stato un errore'
  },
  components: {
    flashes: {
      verifyEmail: 'Prenditi un attimo di tempo per verificare il tuo indirizzo email.',
      verifyEmailBtnLabel: 'Reinvia email di verifica',
      verifyEmailSuccess: 'Una mail è stata spedita al tuo indirizzo. Clicca sul link contenuto nel m,essaggio per verificare il tuo indirizzo.',
      pendingInvitations: 'Hai degli inviti in sospeso',
      pendingSync: 'Sincronizzazione del libro in sospeso',
      goBookShelf: 'Vai alla libreria'
    },
    userSidebarWidget: {
      navItems: {
        dashboard: 'Bacheca',
        personalInfo: 'Info personali',
        groups: 'Gruppi',
        comments: 'Commenti'
      },
      adminNavItems: {
        adminDashboard: 'Bacheca Admin',
        users: 'Utenti',
        books: 'Libri'
      }
    },
    mainNav: {
      bookShelf: 'Libreria'
    },
    userNavWidget: {
      howdy: 'Ciao!',
      awesomeExperience: 'Benvenuto nell\'entusiasmante esperienza di Social Books Creator.',
      toGetStarted: 'Per cominciare, per favore',
      accessProfile: 'Accedi al tuo profilo ed alle tue impostazioni.',
      profile: 'Profilo',
      editorSettings: 'Impostazioni editor'
    },
    personalDataForm: {
      heading: 'I tuoi dati personali'
    },
    downloadPersonalData: {
      json: 'Scarica in formato JSON',
      info: 'Lorem ipsum dolor sit amet etc etc'
    },
    avatarWidget: {
      heading: 'Impostazioni avatar'
    },
    loginForm: {
      heading: 'Form di login',
      noAccount: 'Non hai un account?'
    },
    registerForm: {
      heading: 'Form di registrazione',
      alreadyAccount: 'Hai già un account?'
    },
    resetPasswordForm: {
      heading: 'Reimposta password',
      pswdRemembered: 'Ti sei ricordato la password?'
    },
    confirmPasswordResetForm: {
      heading: 'Scegli una nuova password',
      confirmPswd: 'Conferma la tua nuova password',
      nowCanLogin: 'Ora puoi accedere con la tua nuova password'
    },
    changePassword: {
      typeOldPwd: 'Inserisci la vecchia password',
      typeNewPwd: 'Inserisci la nuova password',
      typeNewPwd2: 'Conferma la nuova password',
      pwdUpdated: 'Password aggiornata!'
    },
    groupList: {
      heading: 'I miei gruppi',
      newGroup: 'Nuovo gruppo'
    },
    newGroupModal: {
      pickName: 'Scegli un nome per il tuo nuovo gruppo',
      inviteCollaborators: 'Invita altri utenti a collaborare a questo gruppo',
      shareBooks: 'Condividi uno o più dei tuoi libri con il gruppo',
      createNewGroup: 'Crea nuovo gruppo',
      groupNameCaption: 'Questo sarà il nome del tuo gruppo (es. "IV A")',
      groupInvitedCaption: 'I membri del gruppo possono visualizzarne l\'attività e modificare i libri condivisi',
      groupSharedBooksCaption: 'Questi saranno i libri su cui il gruppo lavorerà. Puoi saltare questo passaggio e creare e condividere un nuovo libro più avanti'
    },
    preferencesWidget: {
      title: 'Preferenze',
      week: 'Ogni settimana',
      weeks2: 'Ogni due settimane',
      month: 'Ogni mese',
      defaultLang: 'Seleziona la lingua principale',
      hints: 'Attiva/disattiva suggerimenti',
      mailFreq: 'Imposta la frequenza di invio email',
      mailFromUs: 'Ricevi le nostre email'
    },
    privacyWidget: {
      title: 'Security & Privacy',
      changePwd: 'Change password',
      dwnldPersonalData: 'Download personal data'
    },
    notifications: {
      syncBookNotification: 'Il libro {text} a cui collabori può essere sincronizzato',
      groupInvitationReceived: 'Sei stato invitato ad unirti al gruppo "{text}"',
      userAcceptInvitation: 'L\'utente {text} ha accettato il tuo invito',
      requestModification: 'L\'utente {text} ha inviato una proposta di modifica ad un libro condiviso',
      ownerAcceptedChanges: 'L\'aaministratore del gruppo {text} ha accettato le tue modifiche',
      yourNotifications: 'Le tue notifiche',
      noNotifications: 'Non ci sono nuove notifiche'
    },
    bookCard: {
      saveTemplate: 'Salva come template',
      duplicate: 'Duplica',
      downloadPdf: 'Download PDF',
      openEditor: 'Apri nell\'editor'
    },
    invitationsWidget: {
      invitations: 'Inviti'
    },
    profileDataForm: {
      nameReq: 'Il campo nome è obbligatorio',
      surnameReq: 'Il campo cognome è obbligatorio',
      bioRules: 'La biografia deve essere lunga al massimo 300 caratteri'
    },
    columnDrawer: {
      editor: 'Editor',
      preview: 'Anteprima',
      chapters: 'Capitoli',
      settings: 'Impostazioni'
    }
  },
  pages: {
    emailLanding: {
      verifyingAddress: 'Un attimo per favore, stiamo controllando il tuo indirizzo email.',
      emailVerified: 'Ottimo! La tua email è stata verificata.',
      emailVerificationeError: 'Ops! Non siamo riusciti a verificare la tua email.'
    },
    bookShelf: {
      noSharedBooks: 'Non hai ancora libri condivisi...',
      sharedBooks: 'Qui troverai i libri a cui stai collaborando.',
      learnMore: 'Approfondisci il flusso di lavoro collaborativo di Social Books Creator...',
      sharedBooksTitle: 'Libri condivisi con te',
      createNewBook: 'Crea un nuovo libro',
      noBooks: 'Non hai ancora libri...',
      startCreating: 'Comincia creandone uno nuovo.',
      addBook: 'Aggiungi un libro',
      addBooks: 'Aggiungi libri',
      manageRequests: 'Gestisci richieste'
    }
  },
  app: {
    bookShelf: {
      yourBooks: 'I tuoi libri'
    },
    editor: {
      bookSettings: {
        sectionLabel: 'Impostazioni libro',
        title: 'Titolo',
        abstract: 'Abstract',
        saved: 'Impostazioni libro salvate...',
        headingFont: 'Carattere titoli',
        size: 'Dimensione',
        bodyFont: 'Carattere corpo del testo',
        pageSize: 'Dimensioni pagina',
        orientation: 'Orientamento pagina',
        formatOptions: 'Opzioni di formattazione',
        license: 'Modifica licenza',
        unpublish: 'Non pubblicare',
        publish: 'Pubblica',
        publishingSettings: 'Impostazioni pubblicazione',
        bookInfo: 'Informazioni sul libro',
        bookCoverEditor: 'Editor della copertina'
      },
      chapters: {
        structureSaved: 'Struttura capitoli salvata...',
        deleteSub: 'Elimina sottocapitolo',
        moveDown: 'Sposta in basso',
        moveUp: 'Sposta in alto',
        subSettings: 'Impostazioni sottocapitolo',
        delete: 'Elimina capitolo',
        settings: 'Impostazioni capitolo',
        addSub: 'Aggiungi sottocapitolo',
        add: 'Aggiungi capitolo'
      },
      layout: {
        placeholder: 'Sccrivi una storia meravigliosa',
        showBlocks: 'Mostra blocchi',
        showColors: 'Mostra colori blocco',
        hideBlocks: 'Nascondi blocchi',
        hideColors: 'Nascondi colori blocchi'
      },
      preview: {
        expand: 'Espandi la sezione per vedere l\'anteprima'
      }
    },
    shared: {
      deleteBook: 'Ok, elimina questo libro',
      deleteSure: 'Sei sicuro di voler procedere?',
      deleteLost: 'Tutto il tuo lavoro su questo libro andrà perso!',
      pickTitle: 'Scegli un titolo per il tuo libro',
      chooseCC: 'Scegli una licenza CC per il tuo libro',
      abstract: 'Scrivi un abstract per il tuo libro',
      createBook: 'Crea il tuo libro',
      bookSummary: 'Un breve riassunto o un testo di presentazione. Non obbligatorio. Può essere saltato.',
      bookLicense: 'Questa è la licenza con cui il tuo libro verrà pubblicato.',
      license: 'Licenza',
      bookTitle: 'Questo è il titolo principale del tuo libro (es. "Moby Dick")'
    }
  },
  group: {
    modsSumitted: 'Le tue modifiche sono state sottoposte all\'amministratore del gruppo!',
    submitChanges: 'Sottoponi le tue modifiche',
    openSyncDialog: 'Apri la finestra di sincronizzazione',
    syncPending: 'Sincronizzazione in sospeso.',
    syncFailed: 'Sincronizzazione fallita',
    syncOk: 'Sincronizzazione completata',
    syncPage: 'Pagina Sincronizzazione',
    masterBookUpdated: 'La copia master di questo libro è stata aggiornata',
    alertBeforeSync: 'Please, be aware that by proceeding, work not yet integrated into the master copy will be lost. Consider submitting it before syncing.',
    completeSync: 'SINCRONIZZAZIONE COMPLETA',
    partialSync: 'SINCRONIZZAZIONE PARZIALE',
    unsubscribe: 'Abbandona questo gruppo',
    reviewDiffs: 'Confronta le differenze',
    diffsMerged: 'Differenze integrate!',
    admin: 'Amministratore',
    members: 'Collaboratori',
    noMembers: 'Non ci sono collaboratori',
    invited: 'Invitato',
    addMember: 'Aggiungi collaboratore',
    noInvitations: 'Non ci sono inviti in sospeso',
    addBookToGroup: 'Aggiungi un libro a questo gruppo',
    userDeleted: 'Utente eliminato!',
    userNotFound: 'L\'utente {user} non è stato trovato!',
    usersInvited: 'Utenti invitati',
    manageRequests: 'Gestisci richieste di modifica',
    inviteAccepted: 'Ora sei un membro del gruppo \'{group}\'!',
    deleteGroup: 'Elimina gruppo',
    viewGroup: 'Vedi gruppo',
    editGroup: 'Modifica gruppo',
    viewChanges: 'Vedi modifiche'
  },
  auth: {
    emailReq: 'Il campo email è obbligatorio',
    pwdReq: 'La password è obbligatoria',
    emailInvalid: 'Indirizzo email non valido',
    pwd6char: 'La password deve essere di almeno 6 caratteri',
    newPwdSet: 'La tua nuova password è stata impostata...',
    repeatPwdReq: 'Il campo "Ripeti password" è obbligatorio',
    pwdMatch: 'Le password devono coincidere',
    resetPwdEmailSent: 'Ti abbiamo inviato una mail per reimpostare la password...',
    pwdUpdated: 'Password aggiornata!'
  }
}
