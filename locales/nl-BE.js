export default {
  shared: {
    login: 'Inloggen',
    logout: 'Uitloggen',
    register: 'Registreren',
    resetPswd: 'Wachtwoord opnieuw instellen',
    forgotPswd: 'Je wachtwoord vergeten?',
    or: 'of',
    welcome: 'Welkom',
    homePage: 'Startpagina',
    home: 'Home',
    save: 'Opslaan',
    reset: 'Opnieuw instellen',
    upload: 'Uploaden',
    remove: 'Verwijderen',
    back: 'Terug',
    forward: 'Doorzenden',
    prev: 'Vorige',
    next: 'Volgend',
    cancel: 'Annuleren',
    delete: 'Verwijderen',
    change: 'Wijziging',
    savingFail: 'Opslaan is mislukt',
    oops: 'Oeps, er is iets misgegaan',
    greatSaved: 'Geweldig, inhoud opgeslagen',
    accept: 'Aanvaarden',
    deny: 'Weigeren',
    confirm: 'Bevestigen',
    formLabels: {
      name: 'Voornaam',
      surname: 'Achternaam',
      email: 'E-mail',
      address: 'Adress',
      bio: 'Biographie',
      password: 'Wachtwoord ',
      passwordRepeat: 'Herhaal wachtwoord',
      newPassword: 'Nieuw wachtwoord',
      groupName: 'Groepsnaam',
      groupInvited: 'Uitgenodigde gebruikers',
      groupMembers: 'Groepsleden',
      groupSharedBooks: 'Gedeelde boeken',
      organization: 'Uw organisatie of school'
    },
    toastMessages: {
      newGroupCreated: 'Super! Je nieuwe groep is klaar om te beginnen.'
    }
  },
  publicFrontend: {
    baseTitle: 'Welkom'
  },
  errorPage: {
    fourZeroFour: '404 Niet Gevonden',
    genericError: 'Er is een fout opgetreden'
  },
  components: {
    flashes: {
      verifyEmail: 'Neem even de tijd om uw e-mailadres te verifiëren.',
      verifyEmailBtnLabel: 'Verzend de verificatie-e-mail opnieuw',
      verifyEmailSuccess: 'Er is een e-mail naar uw adres gestuurd. Klik op de link in het bericht om uw e-mailadres te verifiëren.',
      pendingInvitations: 'Je hebt openstaande uitnodigingen',
      pendingSync: 'Boeksynchronisatie in behandeling',
      goBookShelf: 'Ga naar de boekenplank'
    },
    userSidebarWidget: {
      navItems: {
        dashboard: 'Dashboard',
        personalInfo: 'Persoonlijke informatie',
        groups: 'Groepen',
        comments: 'Opmerkingen'
      },
      adminNavItems: {
        adminDashboard: 'Beheerdersdashboard',
        users: 'Gebruikers',
        books: 'Boeken'
      }
    },
    mainNav: {
      bookShelf: 'Boekenplank'
    },
    userNavWidget: {
      howdy: 'Hallo gast!',
      awesomeExperience: 'Welkom bij de geweldige Social Books Creator-ervaring.',
      toGetStarted: 'Om te beginnen, alstublieft',
      accessProfile: 'Toegang tot uw profiel en instellingen.',
      profile: 'Profiel',
      editorSettings: 'Editor-instellingen'
    },
    personalDataForm: {
      heading: 'Uw persoonlijke gegevens'
    },
    downloadPersonalData: {
      json: 'JSON downloaden',
      info: 'Lorem ipsum dolor sit amet etc etc'
    },
    avatarWidget: {
      heading: 'Avatar instellingen'
    },
    loginForm: {
      heading: 'Login formulier',
      noAccount: 'Heb je geen account?'
    },
    registerForm: {
      heading: 'Inschrijfformulier',
      alreadyAccount: 'Heeft u al een account?'
    },
    resetPasswordForm: {
      heading: 'Wachtwoord resetten',
      pswdRemembered: 'Herinnert u zich uw wachtwoord?'
    },
    confirmPasswordResetForm: {
      heading: 'Kies een nieuw wachtwoord',
      confirmPswd: 'Bevestig je nieuwe wachtwoord',
      nowCanLogin: 'Nu kunt u inloggen met uw nieuwe wachtwoord'
    },
    changePassword: {
      typeOldPwd: 'Typ je oude wachtwoord',
      typeNewPwd: 'Typ je nieuwe wachtwoord',
      typeNewPwd2: 'Bevestig je nieuwe wachtwoord',
      pwdUpdated: 'Wachtwoord bijgewerkt!'
    },
    groupList: {
      heading: 'Mijn groepen',
      newGroup: 'Nieuwe groep'
    },
    newGroupModal: {
      pickName: 'Kies een naam voor je nieuwe groep',
      inviteCollaborators: 'Nodig andere gebruikers uit om samen te werken aan deze groep',
      shareBooks: 'Deel een of meer van je boeken met deze groep',
      createNewGroup: 'Een nieuwe groep instellen',
      groupNameCaption: 'Dit zal de naam van de groep (bijv. "IVe jaar")',
      groupInvitedCaption: 'Groepsleden kunnen deze groepsactiviteit bekijken en gedeelde boeken bewerken',
      groupSharedBooksCaption: 'Dit zijn de boeken waaraan de groep zal werken. Je kunt deze stap overslaan en later een nieuw boek maken en delen'
    },
    preferencesWidget: {
      title: 'Voorkeuren',
      week: 'Elke week',
      weeks2: 'Elke twee weken',
      month: 'Elke maand',
      defaultLang: 'Selecteer uw standaardtaal',
      hints: 'Schakel hints in',
      mailFreq: 'Stel uw mailfrequentie in',
      mailFromUs: 'Ontvang mail van ons'
    },
    privacyWidget: {
      title: 'Beveiliging en privacy',
      changePwd: 'Wachtwoord wijzigen',
      dwnldPersonalData: 'Download persoonlijke gegevens'
    },
    notifications: {
      syncBookNotification: 'Je gedeelde boek {text} kan worden gesynchroniseerd',
      groupInvitationReceived: 'Je wordt uitgenodigd om lid te worden van de groep "{text}"',
      userAcceptInvitation: 'De gebruiker {text} heeft uw uitnodiging geaccepteerd',
      requestModification: 'Gebruiker {text} heeft een wijziging naar een tekstboek gestuurd',
      ownerAcceptedChanges: 'De beheerder van de {text} groep heeft uw wijzigingen geaccepteerd',
      yourNotifications: 'Jouw notificaties',
      noNotifications: 'Je hebt geen meldingen'
    },
    bookCard: {
      saveTemplate: 'Opslaan als sjabloon',
      duplicate: 'Dupliceren',
      downloadPdf: 'Download PDF',
      openEditor: 'Openen in Editor'
    },
    invitationsWidget: {
      invitations: 'Invitations'
    },
    profileDataForm: {
      nameReq: 'Naam is vereist',
      surnameReq: 'Achternaam is verplicht',
      bioRules: 'Biografie moet kleiner zijn dan of gelijk zijn aan 300 tekens'
    },
    columnDrawer: {
      editor: 'Editor',
      preview: 'Voorbeeld',
      chapters: 'Hoofdstukken',
      settings: 'Instellingen'
    }
  },
  pages: {
    emailLanding: {
      verifyingAddress: 'Wacht even, we controleren uw e-mailadres.',
      emailVerified: 'Super! Je e-mailadres is geverifieerd.',
      emailVerificationeError: 'Ops! We kunnen uw e-mailadres niet verifiëren.'
    },
    bookShelf: {
      noSharedBooks: 'Je hebt nog geen gedeelde boeken ...',
      sharedBooks: 'Hier vind je de boeken waaraan je samenwerkt.',
      learnMore: 'Meer informatie over de collaboratieve workflow van Social Books Creator ...',
      sharedBooksTitle: 'Boeken die met jou zijn gedeeld',
      createNewBook: 'Maak een nieuw boek',
      noBooks: 'Nog geen boeken ...',
      startCreating: 'Begin met het maken van een nieuwe.',
      addBook: 'Boek toevoegen',
      addBooks: 'Boeken toevoegen',
      manageRequests: 'Beheer verzoeken'
    }
  },
  app: {
    bookShelf: {
      yourBooks: 'Jouw boeken'
    },
    editor: {
      bookSettings: {
        sectionLabel: 'Boek instellingen',
        title: 'Titel',
        abstract: 'Abstract',
        saved: 'Boekinstellingen opgeslagen...',
        headingFont: 'Koppen lettertype',
        size: 'Grootte',
        bodyFont: 'Body lettertype',
        pageSize: 'Pagina grootte',
        orientation: 'Paginaoriëntatie',
        formatOptions: 'Opmaakopties',
        license: 'Licentie bewerken',
        unpublish: 'Publicatie ongedaan maken',
        publish: 'Publiceren',
        publishingSettings: 'Publicatie-instellingen',
        bookInfo: 'Boek info',
        bookCoverEditor: 'Cover editor'
      },
      chapters: {
        structureSaved: 'Hoofdstukstructuur opgeslagen ...',
        deleteSub: 'Subhoofdstuk verwijderen',
        moveDown: 'Naar beneden gaan ',
        moveUp: 'Naar boven gaan',
        subSettings: 'Subhoofdstuk instellingen',
        delete: 'Hoofdstuk verwijderen',
        settings: 'Hoofdstuk instellingen',
        addSub: 'Subhoofdstuk toevoegen',
        add: 'Hoofdstuk toevoegen'
      },
      layout: {
        placeholder: 'Laten we een geweldig verhaal schrijven!',
        showBlocks: 'Toon blokken',
        showColors: 'Toon kleuren',
        hideBlocks: 'Hide blocks',
        hideColors: 'Hide colors'
      },
      preview: {
        expand: 'Breid het hoofdstuk uit naar de preview '
      }
    },
    shared: {
      deleteBook: 'Ja, verwijder dit boek',
      deleteSure: 'Weet je zeker dat je verder wilt gaan?',
      deleteLost: 'Al je werk aan dit boek gaat verloren!',
      pickTitle: 'Kies een titel voor je boek',
      chooseCC: 'Kies een CC-licentie voor uw boek',
      abstract: 'Geef een samenvatting voor je boek',
      createBook: 'Maak je boek',
      bookSummary: 'Een korte samenvatting of presentatietekst. Niet verplicht. Kan worden overgeslagen.',
      bookLicense: 'Dit is de licentie waarmee uw boek wordt gepubliceerd.',
      license: 'Licentie',
      bookTitle: 'Dit is de hoofdtitel van je boek (bijvoorbeeld \'Moby Dick\')'
    }
  },
  group: {
    modsSumitted: 'Uw wijzigingen zijn ingediend bij de groepseigenaar!',
    submitChanges: 'Dien uw wijzigingen in',
    openSyncDialog: 'Open Sync Dialogue',
    syncPending: 'In afwachting van synchronisatie',
    syncFailed: 'Synchronisatie mislukt',
    syncOk: 'Synchronisatie voltooid',
    syncPage: 'Pagina synchroniseren',
    masterBookUpdated: 'Masterexemplaar van dit boek is geüpdatet',
    alertBeforeSync: 'Houd er rekening mee dat als u doorgaat, werk dat nog niet is geïntegreerd in het masterkopie, verloren gaat. Overweeg om het in te dienen voordat u het synchroniseert.',
    completeSync: 'Volledige synchronisatie',
    partialSync: 'Gedeeltelijke synchronisatie',
    unsubscribe: 'Afmelden bij deze groep',
    reviewDiffs: 'Bekijk verschillen',
    diffsMerged: 'Verschillen samengevoegd',
    admin: 'Beheerder',
    members: 'Leden',
    noMembers: 'Er zijn geen leden',
    invited: 'Uitgenodigd',
    addMember: 'Lid toevoegen',
    noInvitations: 'Er zijn geen uitnodigingen in behandeling',
    addBookToGroup: 'Boek toevoegen aan deze groep',
    userDeleted: 'Gebruiker verwijderd!',
    userNotFound: 'Gebruiker {user} niet gevonden!',
    usersInvited: 'Gebruikers uitgenodigd',
    manageRequests: 'Bekijk veranderingen',
    inviteAccepted: 'Je bent nu lid van de \'{group}\' groep!',
    deleteGroup: 'Groep verwijderen',
    viewGroup: 'Groep bekijken',
    editGroup: 'Groep bewerken',
    viewChanges: 'Bekijk veranderingen'
  },
  auth: {
    emailReq: 'E-mail is verplicht',
    pwdReq: 'Een wachtwoord is verplicht',
    emailInvalid: 'Ongeldig e-mailadres',
    pwd6char: 'Wachtwoord moet minimaal 6 tekens bevatten',
    newPwdSet: 'Uw nieuwe wachtwoord is ingesteld ...',
    repeatPwdReq: 'Herhaal wachtwoord is vereist',
    pwdMatch: 'Wachtwoorden moeten overeenkomen',
    resetPwdEmailSent: 'E-mail voor wachtwoord opnieuw instellen verzonden',
    pwdUpdated: 'Wachtwoord bijgewerkt!'
  }
}
