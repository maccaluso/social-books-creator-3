export default {
  shared: {
    login: 'Acceder',
    logout: 'Salir',
    register: 'Registrarse',
    resetPswd: 'Restablecer la contraseña',
    forgotPswd: '¿Olvidaste tu contraseña?',
    or: 'o',
    welcome: 'Bienvenido',
    homePage: 'Página de inicio',
    home: 'Inicio',
    save: 'Guardar',
    reset: 'Reiniciar',
    upload: 'Subir',
    remove: 'Quitar',
    back: 'Atrás',
    forward: 'Adelante',
    prev: 'Anterior',
    next: 'Posterior',
    cancel: 'Cancelar',
    delete: 'Eliminar',
    change: 'Cambiar',
    savingFail: 'No se ha podido guardar',
    oops: 'Oh, algo ha fallado',
    greatSaved: '¡Genial!, ¡guardado con éxito!',
    accept: 'Aceptar',
    deny: 'Rechazar',
    confirm: 'Confirmar',
    formLabels: {
      name: 'Nombre',
      surname: 'Apellido',
      email: 'Email',
      address: 'Dirección',
      bio: 'Biografía',
      password: 'Contraseña',
      passwordRepeat: 'Repetir contraseña',
      newPassword: 'Nueva contraseña',
      groupName: 'Nombre del grupo',
      groupInvited: 'Usuarios invitados',
      groupMembers: 'Miembros del grupo',
      groupSharedBooks: 'Libros compartidos',
      organization: 'Tu organización o escuela'
    },
    toastMessages: {
      newGroupCreated: '¡Fantástico! Tu nuevo grupo está listo para empezar.'
    }
  },
  publicFrontend: {
    baseTitle: 'Bienvenido'
  },
  errorPage: {
    fourZeroFour: '404 página no encontrada',
    genericError: 'Se ha producido un error.'
  },
  components: {
    flashes: {
      verifyEmail: 'Por favor, verifica tu dirección de correo.',
      verifyEmailBtnLabel: 'Re-envía la verificación del email.',
      verifyEmailSuccess: 'Te hemos enviado un email a tu cuenta. Pica en el enlace para verificar tu email.',
      pendingInvitations: 'Tienes invitaciones pendientes',
      pendingSync: 'Sincronización del libro pendiente',
      goBookShelf: 'Ve a la librería'
    },
    userSidebarWidget: {
      navItems: {
        dashboard: 'Dashboard',
        personalInfo: 'Información personal.',
        groups: 'Grupos',
        comments: 'Comentarios'
      },
      adminNavItems: {
        adminDashboard: 'Admimnistrar dashboard',
        users: 'Usuarios',
        books: 'Libros'
      }
    },
    mainNav: {
      bookShelf: 'Librería'
    },
    userNavWidget: {
      howdy: 'Hola invitado!!',
      awesomeExperience: 'Bienvenido/a a la maravillosa experiencia del Social Books Creator.',
      toGetStarted: 'Para comenzar, por favor',
      accessProfile: 'Accede a tu perfil y a ajustes.',
      profile: 'Perfil',
      editorSettings: 'Ajustes de edición'
    },
    personalDataForm: {
      heading: 'Tu información personal'
    },
    downloadPersonalData: {
      json: 'Descargar JSON',
      info: 'Lorem ipsum dolor sit amet etc etc'
    },
    avatarWidget: {
      heading: 'Ajustes de Avatar'
    },
    loginForm: {
      heading: 'Formulario de ingreso',
      noAccount: '¿No tienes una cuenta?'
    },
    registerForm: {
      heading: 'Formulario de registro',
      alreadyAccount: '¿Ya tienes una cuenta?'
    },
    resetPasswordForm: {
      heading: 'Reestablece contraseña',
      pswdRemembered: '¿Olvidaste tu contraseña?'
    },
    confirmPasswordResetForm: {
      heading: 'Elige una nueva contraseña',
      confirmPswd: 'Confirma tu nueva contraseña',
      nowCanLogin: 'Ahora puedes acceder con tu nueva contraseña'
    },
    changePassword: {
      typeOldPwd: 'Escribe tu contraseña actual',
      typeNewPwd: 'Escribe tu nueva contraseña',
      typeNewPwd2: 'Confirma tu nueva contraseña',
      pwdUpdated: 'Contraseña actualizada'
    },
    groupList: {
      heading: 'Mis grupos',
      newGroup: 'Grupo nuevo'
    },
    newGroupModal: {
      pickName: 'Elige un nombre para tu grupo nuevo.',
      inviteCollaborators: 'Invita a otros usuarios para colaborar en este grupo.',
      shareBooks: 'Comparte uno o mas de tus libros con este grupo.',
      createNewGroup: 'Crea un grupo nuevo.',
      groupNameCaption: 'Este será el nombre de tu grupo (e.g. "IVth yr")',
      groupInvitedCaption: 'Los miembros del grupo podrán ver la actividad del grupo y editar los libros compartidos',
      groupSharedBooksCaption: 'Estos serán los libros con los que el grupo trabajará. Puedes saltarte este paso y crear un grupo nuevo más tarde.'
    },
    preferencesWidget: {
      title: 'Preferencias',
      week: 'Cada semana',
      weeks2: 'Cada dos semanas',
      month: 'Cada mes',
      defaultLang: 'Selecciona tu idioma',
      hints: 'Habilitar/deshabilitar sugerencias',
      mailFreq: 'Configura frecuencia de correos',
      mailFromUs: 'Recibe nuestros correos'
    },
    privacyWidget: {
      title: 'Seguridad y privacidad',
      changePwd: 'Cambiar contraseña',
      dwnldPersonalData: 'Descargar datos personales'
    },
    notifications: {
      syncBookNotification: 'Tu libro {text} compartido puede ser sincronizado',
      groupInvitationReceived: 'Te han invitado a unirte al grupo {text}',
      userAcceptInvitation: 'El usario {text} ha aceptado tu invitación',
      requestModification: 'El usario {text} mandó un cambio a tu libro compartido',
      ownerAcceptedChanges: 'el administrador del grupo {text} ha aceptado tus cambios',
      yourNotifications: 'Tus notificaciones',
      noNotifications: 'No tienes notificaciones'
    },
    bookCard: {
      saveTemplate: 'Guardar como plantilla',
      duplicate: 'Duplicar',
      downloadPdf: 'Descargar PDF',
      openEditor: 'Abrir en el editor'
    },
    invitationsWidget: {
      invitations: 'Invitaciones'
    },
    profileDataForm: {
      nameReq: 'Se requiere un nombre',
      surnameReq: 'Se requiere un apellido',
      bioRules: 'La biografía debe ser menor o igual a 300 caracteres'
    },
    columnDrawer: {
      editor: 'Editor',
      preview: 'Avance',
      chapters: 'Capítulos',
      settings: 'Configuraciones'
    }
  },
  pages: {
    emailLanding: {
      verifyingAddress: 'Por favor, espera un momento, estamos comprobando tu cuenta de correo.',
      emailVerified: 'Tu cuenta de correo ha sido verificada.',
      emailVerificationeError: 'Lo sentimos, no hemos podido verificar tu cuenta de correo.'
    },
    bookShelf: {
      noSharedBooks: 'No has compartido ningún libro por ahora',
      sharedBooks: 'Aquí encontrarás los libros en los que estás colaborando.',
      learnMore: 'Aprende más sobre el flujo de trabajo colaborativo de Social Books Creator',
      sharedBooksTitle: 'Libros compartidos contigo',
      createNewBook: 'Crea un nuevo libro',
      noBooks: 'No hay libros aún',
      startCreating: 'Empieza creando uno nuevo',
      addBook: 'Añadir libro',
      addBooks: 'Añadir libros',
      manageRequests: 'Gestionar peticiones'
    }
  },
  app: {
    bookShelf: {
      yourBooks: 'Tus libros'
    },
    editor: {
      bookSettings: {
        sectionLabel: 'Ajustes del libro',
        title: 'Título',
        abstract: 'Resumen',
        saved: 'Ajustes del libro guardados',
        headingFont: 'Fuentes del títular',
        size: 'Tamaño',
        bodyFont: 'Fuentes del cuerpo',
        pageSize: 'Tamaño de página',
        orientation: 'Orientación de página',
        formatOptions: 'Opciones de formateo',
        license: 'Editar licencia',
        unpublish: 'Eliminar publicación',
        publish: 'Publicar',
        publishingSettings: 'Publicar ajustes',
        bookInfo: 'Información del libro',
        bookCoverEditor: 'Cover editor'
      },
      chapters: {
        structureSaved: 'Estructura del capítulo guardada',
        deleteSub: 'Eliminar sub-capítulo',
        moveDown: 'Mover abajo',
        moveUp: 'Mover arriba',
        subSettings: 'Ajustes del sub-capítulo',
        delete: 'Eliminar capítulo',
        settings: 'Ajutes del capítulo',
        addSub: 'Añadir sub-capítulo',
        add: 'Añadir capítulo'
      },
      layout: {
        placeholder: '¡Vamos a escribir una historia increible!!',
        showBlocks: 'Mostrar bloques',
        showColors: 'Mostrar colores',
        hideBlocks: 'Hide blocks',
        hideColors: 'Hide colors'
      },
      preview: {
        expand: 'Expandir la sección para previsualización'
      }
    },
    shared: {
      deleteBook: 'Sí, borrar este libro',
      deleteSure: '¿Seguro que quieres continuar?',
      deleteLost: 'Todo tu trabajo en este libro se eliminará',
      pickTitle: 'Elige un título para tu libro',
      chooseCC: 'Elige una licencia CC para tu libro',
      abstract: 'Haz un resumen de tu libro',
      createBook: 'Crea tu libro',
      bookSummary: 'Un breve resumen o texto de presentación. No es obligatorio. Salta este paso.',
      bookLicense: 'Esta es la licencia con la cual tu libro será publicado.',
      license: 'Licencia',
      bookTitle: 'Este es el principal título de tu libro (ex. Moby Dick)'
    }
  },
  group: {
    modsSumitted: 'Tus cambios han sido enviados al propietario del grupo',
    submitChanges: 'Enviar tus cambios',
    openSyncDialog: 'Abrir diálogo de sincronización',
    syncPending: 'Sincronización pendiente',
    syncFailed: 'Error en la sincronización',
    syncOk: 'Sincronización completada',
    syncPage: 'Sincronizar página',
    masterBookUpdated: 'La copia maestra de este libro ha sido actualizada',
    alertBeforeSync: 'Por favor, tenga en cuenta que al proceder, el trabajo, que no haya sido integrado en la copia maestra, se perderá. Reconsidera enviar antes de sincronizar.',
    completeSync: 'Sincronización completa',
    partialSync: 'Sincronización parcial',
    unsubscribe: 'Salir de este grupo',
    reviewDiffs: 'Revisar diferencias',
    diffsMerged: 'Diferencias fusionadas',
    admin: 'administar',
    members: 'Miembros ',
    noMembers: 'No hay miembros',
    invited: 'Invitado',
    addMember: 'Añadir miembros',
    noInvitations: 'No hay invitaciones pendientes',
    addBookToGroup: 'Añadir libro a este grupo',
    userDeleted: 'Usuario borrado',
    userNotFound: 'Usuario {user} no encontrado',
    usersInvited: 'Usuarios invitados',
    manageRequests: 'Gestionar solicitudes',
    inviteAccepted: 'Ahora eres miembro del grupo \'{group}\'!',
    deleteGroup: 'Eliminar grupo',
    viewGroup: 'Ver grupo',
    editGroup: 'Editar grupo',
    viewChanges: 'Ver cambios'
  },
  auth: {
    emailReq: 'Se requiere Email',
    pwdReq: 'Se requiere contraseña',
    emailInvalid: 'Dirección de correos invalida',
    pwd6char: 'La contraseña debe contener al menos 6 caracteres',
    newPwdSet: 'Tu nueva contraseña ha sido establecida',
    repeatPwdReq: 'Repite la contraseña',
    pwdMatch: 'Las contraseñas deben coincidir',
    resetPwdEmailSent: 'Contraseña restablecida, email enviado',
    pwdUpdated: 'Contraseña actualizada'
  }
}
