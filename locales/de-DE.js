export default {
  shared: {
    login: 'Anmelden',
    logout: 'Abmelden',
    register: 'Registrieren',
    resetPswd: 'Passwort zurücksetzen',
    forgotPswd: 'Passwort vergessen?',
    or: 'oder',
    welcome: 'Willkommen',
    homePage: 'Webseite',
    home: 'Start',
    save: 'speichern',
    reset: 'Zurücksetzen',
    upload: 'hochladen',
    remove: 'entfernen',
    back: 'zurück',
    forward: 'weiter',
    prev: 'vorherige',
    next: 'nächste',
    cancel: 'löschen',
    delete: 'Entfernen',
    change: 'ändern',
    savingFail: 'Speichern fehlgeschlagen',
    oops: 'Hoppla, etwas ist schief gelaufen',
    greatSaved: 'Großartig! Inhalt gespeichert',
    accept: 'Akzeptieren',
    deny: 'Ablehnen',
    confirm: 'Bestätigen',
    formLabels: {
      name: 'Vorname',
      surname: 'Nachname',
      email: 'E-Mail',
      address: 'Adresse',
      bio: 'Biographie',
      password: 'Passwort',
      passwordRepeat: 'Passwort wiederholen',
      newPassword: 'Neues Passwort',
      groupName: 'Gruppenname',
      groupInvited: 'Eingeladene Nutzer',
      groupMembers: 'Gruppenmitglieder',
      groupSharedBooks: 'Geteilte Bücher',
      organization: 'Ihre Organisation oder Schule'
    },
    toastMessages: {
      newGroupCreated: 'Super! Ihre Gruppe ist startbereit.'
    }
  },
  publicFrontend: {
    baseTitle: 'Willkommen'
  },
  errorPage: {
    fourZeroFour: '404 Nicht gefunden',
    genericError: 'Ein Fehler ist aufgetreten'
  },
  components: {
    flashes: {
      verifyEmail: 'Bitte nehmen Sie sich kurz Zeit, um Ihre E-Mail Adresse zu bestätigen.',
      verifyEmailBtnLabel: 'E-Mail zur Überprüfung erneut senden',
      verifyEmailSuccess: 'Eine E-Mail wurde an Ihre Adresse geschickt. Klicken Sie auf den in der Nachricht angegebenen Link, um Ihre E-Mail zu bestätigen.',
      pendingInvitations: 'Sie haben ausstehende Einladungen',
      pendingSync: 'Buchsynchronisation ausstehend',
      goBookShelf: 'Zum Bücherregal gehen'
    },
    userSidebarWidget: {
      navItems: {
        dashboard: 'Dashboard',
        personalInfo: 'Persönliche Daten',
        groups: 'Gruppen',
        comments: 'Kommentare'
      },
      adminNavItems: {
        adminDashboard: 'Admin dashboard',
        users: 'Nutzer',
        books: 'Bücher'
      }
    },
    mainNav: {
      bookShelf: 'Bücherregal'
    },
    userNavWidget: {
      howdy: 'Hallo Gast!',
      awesomeExperience: 'Willkommen bei der großartigen Social Books Creator-Erfahrung.',
      toGetStarted: 'Bitte anfangen',
      accessProfile: 'Zugang zu Ihrem Profil und Ihren Einstellungen.',
      profile: 'Profil',
      editorSettings: 'Editor-Einstellungen'
    },
    personalDataForm: {
      heading: 'Ihre persönlichen Daten'
    },
    downloadPersonalData: {
      json: 'JSON downloaden',
      info: 'Lorem ipsum dolor sit amet etc etc'
    },
    avatarWidget: {
      heading: 'Avatar-Einstellungen'
    },
    loginForm: {
      heading: 'Anmeldeformular',
      noAccount: 'Sie haben kein Konto?'
    },
    registerForm: {
      heading: 'Registrieren',
      alreadyAccount: 'Haben Sie bereits ein Konto?'
    },
    resetPasswordForm: {
      heading: 'Passwort zurücksetzen',
      pswdRemembered: 'Erinnern Sie sich an Ihr Passwort?'
    },
    confirmPasswordResetForm: {
      heading: 'Wählen Sie ein neues Passwort',
      confirmPswd: 'Bestätigen Sie Ihr neues Passwort',
      nowCanLogin: 'Jetzt können Sie sich mit Ihrem neuen Passwort anmelden'
    },
    changePassword: {
      typeOldPwd: 'Geben Sie Ihr altes Passwort ein',
      typeNewPwd: 'Geben Sie Ihr neuesPasswort ein',
      typeNewPwd2: 'Bestätigen Sie Ihr neues Passwort',
      pwdUpdated: 'Passwort geändert!'
    },
    groupList: {
      heading: 'Meine Gruppen',
      newGroup: 'Neue Gruppe'
    },
    newGroupModal: {
      pickName: 'Wählen Sie einen Namen für Ihre neue Gruppe',
      inviteCollaborators: 'Laden Sie andere Nutzer ein, in dieser Gruppe mitzuarbeiten',
      shareBooks: 'Teilen Sie eines oder mehrere Ihrer Bücher mit dieser Gruppe',
      createNewGroup: 'Neue Gruppe erstellen',
      groupNameCaption: 'Dies wird der Name Ihrer Gruppe sein (z.B. "IV. Jahr")',
      groupInvitedCaption: 'Gruppenmitglieder können diese Gruppenaktivität sehen und gemeinsam genutzte Bücher bearbeiten',
      groupSharedBooksCaption: 'Dies werden die Bücher sein, an denen die Gruppe arbeiten wird. Sie können diesen Schritt überspringen und später ein neues Buch erstellen und freigeben'
    },
    preferencesWidget: {
      title: 'Einstellungen',
      week: 'Wöchentlich',
      weeks2: 'Alle zwei Wochen',
      month: 'Monatlich',
      defaultLang: 'Wählen Sie Ihre bevorzugte Sprache',
      hints: 'Hinweise zum Umschalten',
      mailFreq: 'Legen Sie Ihre E-Mail-Frequenz fest',
      mailFromUs: 'Empfangen Sie E-Mail von uns'
    },
    privacyWidget: {
      title: 'Sicherheit & Datenschutz',
      changePwd: 'Passwort ändern',
      dwnldPersonalData: 'Persönliche Daten herunterladen'
    },
    notifications: {
      syncBookNotification: 'Ihr gemeinsames Buch {text} kann synchronisiert werden',
      groupInvitationReceived: 'Sie sind eingeladen, der Gruppe "{text}" beizutreten',
      userAcceptInvitation: 'Der Benutzer {text} hat Ihre Einladung angenommen',
      requestModification: 'Benutzer {text} hat eine Änderung an einem freigegebenen Buch gesendet',
      ownerAcceptedChanges: 'Der Administrator der Gruppe {text} hat Ihre Änderungen akzeptiert',
      yourNotifications: 'Ihre Mitteilungen',
      noNotifications: 'Sie haben keine Mitteilungen'
    },
    bookCard: {
      saveTemplate: 'Als Vorlage speichern',
      duplicate: 'Duplikat',
      downloadPdf: 'Download PDF',
      openEditor: 'Im Editor öffnen'
    },
    invitationsWidget: {
      invitations: 'Einladungen'
    },
    profileDataForm: {
      nameReq: 'Name ist erforderlich',
      surnameReq: 'Familienname ist erforderlich',
      bioRules: 'Die Biographie sollte 300 Zeichen oder weniger betragen'
    },
    columnDrawer: {
      editor: 'Editor',
      preview: 'Vorschau',
      chapters: 'Kapitel',
      settings: 'Einstellungen'
    }
  },
  pages: {
    emailLanding: {
      verifyingAddress: 'Bitte warten Sie einen Moment, wir überprüfen Ihre E-Mail-Adresse.',
      emailVerified: 'Super! Ihre E-Mail wurde bestätigt.',
      emailVerificationeError: 'Hoppla! Wir können Ihre E-Mail nicht bestätigen.'
    },
    bookShelf: {
      noSharedBooks: 'Sie haben noch keine gemeinsamen Bücher...',
      sharedBooks: 'Hier finden Sie die Bücher, an denen Sie mitarbeiten.',
      learnMore: 'Erfahren Sie mehr über den gemeinschaftlichen Arbeitsablauf von Social Books Creator...',
      sharedBooksTitle: 'Mit Ihnen geteilte Bücher',
      createNewBook: 'Neues Buch erstellen',
      noBooks: 'Bisher keine Bücher...',
      startCreating: 'Beginnen Sie mit der Erstellung eines neuen.',
      addBook: 'Buch hinzufügen',
      addBooks: 'Bücher hinzufügen',
      manageRequests: 'Anfragen verwalten'
    }
  },
  app: {
    bookShelf: {
      yourBooks: 'Ihre Bücher'
    },
    editor: {
      bookSettings: {
        sectionLabel: 'Buch-Einstellungen',
        title: 'Titel',
        abstract: 'Kurzfassung',
        saved: 'Buch-Einstellungen gespeichert...',
        headingFont: 'Schriftart der Überschriften',
        size: 'Größe',
        bodyFont: 'Schriftart',
        pageSize: 'Seitengröße',
        orientation: 'Seitenausrichtung',
        formatOptions: 'Formatierungsoptionen',
        license: 'Lizenz bearbeiten',
        unpublish: 'Nicht veröffentlichen',
        publish: 'Veröffentlichen',
        publishingSettings: 'Einstellungen für die Veröffentlichung',
        bookInfo: 'Informationen zum Buch',
        bookCoverEditor: 'Cover editor'
      },
      chapters: {
        structureSaved: 'Kapitelstruktur gespeichert...',
        deleteSub: 'Unterkapitel löschen',
        moveDown: 'Nach unten gehen',
        moveUp: 'Nach oben gehen',
        subSettings: 'Unterkapitel Einstellungen',
        delete: 'Kapitel löschen',
        settings: 'Kapitel-Einstellungen',
        addSub: 'Unterkapitel hinzufügen',
        add: 'Kapitel hinzufügen'
      },
      layout: {
        placeholder: 'Lasst uns eine tolle Geschichte schreiben!',
        showBlocks: 'Blöcke anzeigen',
        showColors: 'Farben anzeigen',
        hideBlocks: 'Hide blocks',
        hideColors: 'Hide colors'
      },
      preview: {
        expand: 'Erweitern Sie den Abschnitt zur Vorschau'
      }
    },
    shared: {
      deleteBook: 'Ja, dieses Buch löschen',
      deleteSure: 'Sind Sie sicher, dass Sie fortfahren wollen?',
      deleteLost: 'All Ihre Arbeit an diesem Buch wird verloren gehen!',
      pickTitle: 'Wählen Sie einen Titel für Ihr Buch',
      chooseCC: 'Wählen Sie eine CC-Lizenz für Ihr Buch',
      abstract: 'Stellen Sie eine Zusammenfassung für Ihr Buch zur Verfügung',
      createBook: 'Erstellen Sie Ihr Buch',
      bookSummary: 'Eine kurze Zusammenfassung oder ein Präsentationstext. Nicht verpflichtend. Kann übersprungen werden.',
      bookLicense: 'Dies ist die Lizenz, mit der Ihr Buch veröffentlicht wird.',
      license: 'Lizenz',
      bookTitle: 'Dies ist der Haupttitel Ihres Buches (z.B. "Moby Dick")'
    }
  },
  group: {
    modsSumitted: 'Ihre Änderungen wurden dem Verantwortlichen der Gruppe weitergeleitet!',
    submitChanges: 'Übermitteln Sie Ihre Änderungen',
    openSyncDialog: 'Sync-Dialog öffnen',
    syncPending: 'Sync ausstehend.',
    syncFailed: 'Sync fehlgeschlagen',
    syncOk: 'Sync abgeschlossen',
    syncPage: 'Sync-Seite',
    masterBookUpdated: 'Das Originalexemplar dieses Buches wurde aktualisiert.',
    alertBeforeSync: 'Bitte beachten Sie, dass durch das Fortfahren noch nicht in die Druckvorlage integrierte Arbeiten verloren gehen. Bitte denken Sie daran, sie vor der Synchronisierung zu übermitteln.',
    completeSync: 'Vollständige Synchronisierung',
    partialSync: 'Teilweise Synchronisierung',
    unsubscribe: 'Von dieser Gruppe abmelden',
    reviewDiffs: 'Unterschiede überprüfen',
    diffsMerged: 'Unterschiede zusammengeführt!',
    admin: 'Administrator',
    members: 'Mitglieder',
    noMembers: 'Es gibt keine Mitglieder',
    invited: 'Eingeladen',
    addMember: 'Mitglied hinzufügen',
    noInvitations: 'Es gibt keine ausstehenden Einladungen',
    addBookToGroup: 'Buch zu dieser Gruppe hinzufügen',
    userDeleted: 'Benutzer gelöscht!',
    userNotFound: 'Benutzer {user} nicht gefunden!',
    usersInvited: 'Eingeladene Benutzer',
    manageRequests: 'Anfragen verwalten',
    inviteAccepted: 'Sie sind jetzt Mitglied der \'{group}\' Gruppe!',
    deleteGroup: 'Gruppe löschen',
    viewGroup: 'Gruppe anzeigen',
    editGroup: 'Gruppe bearbeiten',
    viewChanges: 'Änderungen anzeigen'
  },
  auth: {
    emailReq: 'E-Mail ist erforderlich',
    pwdReq: 'Passwort ist erforderlich',
    emailInvalid: 'Ungültige E-Mail-Adresse',
    pwd6char: 'Das Passwort sollte mindestens 6 Zeichen lang sein',
    newPwdSet: 'Ihr neues Passwort wurde festgelegt...',
    repeatPwdReq: 'Wiederholung des Passworts ist erforderlich',
    pwdMatch: 'Passwörter müssen übereinstimmen',
    resetPwdEmailSent: 'E-Mail zum Zurücksetzen des Passworts gesendet...',
    pwdUpdated: 'Passwort geändert!'
  }
}
