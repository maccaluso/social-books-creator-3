export default {
  shared: {
    login: 'Logare',
    logout: 'Delogare',
    register: 'Înregistrare',
    resetPswd: 'Schimbă parola',
    forgotPswd: 'Am uitat parola',
    or: 'sau',
    welcome: 'Bun venit!',
    homePage: 'Pagina de start',
    home: 'Start',
    save: 'Salvare',
    reset: 'Resetare',
    upload: 'Încarcă',
    remove: 'Șterge',
    back: 'Înapoi',
    forward: 'Înainte',
    prev: 'Anteriorul',
    next: 'Următorul',
    cancel: 'Anuleaza',
    delete: 'Sterge',
    change: 'Schimba',
    savingFail: 'Salvare esuata',
    oops: 'Ooops, ceva nu a mers bine',
    greatSaved: 'Bravo! Continut salvat',
    accept: 'Accepta',
    deny: 'Refuza',
    confirm: 'Confirma',
    formLabels: {
      name: 'Nume',
      surname: 'Prenume',
      email: 'Email',
      address: 'Adresa',
      bio: 'Biografie',
      password: 'Parolă',
      passwordRepeat: 'Rescrie parola',
      newPassword: 'Parola nouă',
      groupName: 'Numele grupului',
      groupInvited: 'Utilizatori invitați',
      groupMembers: 'Membrii grupului',
      groupSharedBooks: 'Cărți distribuite',
      organization: 'Organizatia sau scoala dvs.'
    },
    toastMessages: {
      newGroupCreated: 'Minunat! Noul tău grup este gata să înceapă!'
    }
  },
  publicFrontend: {
    baseTitle: 'Bun venit'
  },
  errorPage: {
    fourZeroFour: '404 Pagina negăsită',
    genericError: 'Eroare'
  },
  components: {
    flashes: {
      verifyEmail: 'Te rugăm să verifici adresa de e-mail',
      verifyEmailBtnLabel: 'Retrimite email-ul de verificare',
      verifyEmailSuccess: 'Apăsați pe link-ul din mesaj pentru a verifica adresa de e-mail.',
      pendingInvitations: 'Aveti invitatii in asteptare',
      pendingSync: 'Sincronizarea cartii in asteptare',
      goBookShelf: 'Mergi la raftul cu carti'
    },
    userSidebarWidget: {
      navItems: {
        dashboard: 'Panou de control',
        personalInfo: 'Informații personale',
        groups: 'Grupuri',
        comments: 'Comentarii'
      },
      adminNavItems: {
        adminDashboard: 'Adminidtrator panou de control',
        users: 'Utilizatori',
        books: 'Cărți'
      }
    },
    mainNav: {
      bookShelf: 'Raftul cu cărți'
    },
    userNavWidget: {
      howdy: 'Salutare!',
      awesomeExperience: 'Bun venit la minunata experiența Social Book Creator',
      toGetStarted: 'Pentru a începe, vă rugăm!',
      accessProfile: 'Accesați profilul și setările',
      profile: 'Profil',
      editorSettings: 'Setări editor'
    },
    personalDataForm: {
      heading: 'Date personale'
    },
    downloadPersonalData: {
      json: 'Descarca JSON',
      info: 'Lorem ipsum dolor sit amet etc etc'
    },
    avatarWidget: {
      heading: 'Setări avatar'
    },
    loginForm: {
      heading: 'Formular de autentificare',
      noAccount: 'Nu aveți cont?'
    },
    registerForm: {
      heading: 'Formular de înregistrare',
      alreadyAccount: 'Aveți deja un cont?'
    },
    resetPasswordForm: {
      heading: 'Resetați parola',
      pswdRemembered: 'Va amintiți parola?'
    },
    confirmPasswordResetForm: {
      heading: 'Alegeți o nouă parola',
      confirmPswd: 'Confirmați parola nouă',
      nowCanLogin: 'Acum vă puteți loga cu noua parolă'
    },
    changePassword: {
      typeOldPwd: 'Tsteaza parola veche',
      typeNewPwd: 'Tasteaza parola noua',
      typeNewPwd2: 'Confirma parola noua',
      pwdUpdated: 'Parola actualizata'
    },
    groupList: {
      heading: 'Grupurile mele',
      newGroup: 'Grup nou'
    },
    newGroupModal: {
      pickName: 'Aleeți un nume pentru noul grup',
      inviteCollaborators: 'Invitați alți utilizatori să colaboreze în acest grup',
      shareBooks: 'Distribuiți una sau mai multe cărți către acest  grup',
      createNewGroup: 'Creați un grup nou',
      groupNameCaption: 'Acesta va fi numele grupului dumneavoastră (ex. anul IV)',
      groupInvitedCaption: 'Membrii grupului pot vizualiza această activitate de grup și pot edita cărțile distribuite.',
      groupSharedBooksCaption: 'Acestea vor fi cărțile la care va lucra grupul. Puteți sări peste acest pas și crea și distribui o carte mai târziu'
    },
    preferencesWidget: {
      title: 'Preferinte',
      week: 'Saptamanal',
      weeks2: 'La fiecare doua saptamani',
      month: 'Lunar',
      defaultLang: 'Selecteaza limba implicita',
      hints: 'Indicii de comutare',
      mailFreq: 'Seteaza frecventa e-mail-urilor',
      mailFromUs: 'Primesti mail de la noi'
    },
    privacyWidget: {
      title: 'Securitate & Confidentialitate',
      changePwd: 'Schimba parola',
      dwnldPersonalData: 'Descarca datele personale'
    },
    notifications: {
      syncBookNotification: 'Cartea {text} ta partajata poate fi sincronizata',
      groupInvitationReceived: 'Esti invitat sa te alaturi grupului "{text}"',
      userAcceptInvitation: 'Utilizatorul {text} a acceptat invitatia',
      requestModification: 'Utilizatorul {text} a trimis o modificare la o carte partajata',
      ownerAcceptedChanges: 'Administratorul {text} grupului a acceptat modificarile tale',
      yourNotifications: 'Notificarile tale',
      noNotifications: 'Nu ai notificari'
    },
    bookCard: {
      saveTemplate: 'Salveaza ca sablon',
      duplicate: 'Duplicare',
      downloadPdf: 'Descarca PDF',
      openEditor: 'Deschide in Editor'
    },
    invitationsWidget: {
      invitations: 'Invitatii'
    },
    profileDataForm: {
      nameReq: 'Se cere numele',
      surnameReq: 'Se cere prenumele',
      bioRules: 'Biografia ar trebui sa aiba mai putin sau sa fie egala cu 300 caractere'
    },
    columnDrawer: {
      editor: 'Editor',
      preview: 'Avanpremieră',
      chapters: 'Capitolele',
      settings: 'Setări'
    }
  },
  pages: {
    emailLanding: {
      verifyingAddress: 'Vă rugăm să așteptați, vă verificăm adresa de e-mail.',
      emailVerified: 'Felicitări! E-mail-ul dumneavoastră a fost verificat cu succes!',
      emailVerificationeError: 'Ops! Nu am reușit să vă verificăm E-mail-ul'
    },
    bookShelf: {
      noSharedBooks: 'Nu ai carti partajate inca',
      sharedBooks: 'Aici vei gasi cartile la care colaborezi',
      learnMore: 'Afla mai multe despre fluxul de lucru colaborativ al Social Book Creator',
      sharedBooksTitle: 'Carti partajete cu tine',
      createNewBook: 'Creaza carte noua',
      noBooks: 'Nici o carte inca...',
      startCreating: 'Incepe prin a crea una noua',
      addBook: 'Adauga carte',
      addBooks: 'Adauga carti',
      manageRequests: 'Gestioneaza cereri'
    }
  },
  app: {
    bookShelf: {
      yourBooks: 'Cartile tale'
    },
    editor: {
      bookSettings: {
        sectionLabel: 'Setarile cartii',
        title: 'Titlu',
        abstract: 'Rezumat',
        saved: 'Setarile cartii salvate',
        headingFont: 'Caraterele rubricilor',
        size: 'Marime',
        bodyFont: 'Body fonturi',
        pageSize: 'Dimensiunea paginii',
        orientation: 'Orientarea paginii',
        formatOptions: 'Optiuni de formatare',
        license: 'Editeaza licenta',
        unpublish: 'Anuleaza publicarea',
        publish: 'Publica',
        publishingSettings: 'Setarile publicarii',
        bookInfo: 'Info despre carte',
        bookCoverEditor: 'Cover editor'
      },
      chapters: {
        structureSaved: 'Structura capitolului salvata',
        deleteSub: 'Sterge subcapitol',
        moveDown: 'Muta in jos',
        moveUp: 'Muta in sus',
        subSettings: 'Setarile subcapitolului',
        delete: 'Sterge capitol',
        settings: 'Setarile capitolului',
        addSub: 'Adauga subcapitol',
        add: 'Adauga capitol'
      },
      layout: {
        placeholder: 'Sa scriem o poveste minunata!',
        showBlocks: 'Arata blocuri',
        showColors: 'Arata culori',
        hideBlocks: 'Hide blocks',
        hideColors: 'Hide colors'
      },
      preview: {
        expand: 'Extinde sectiunea pentru previzionare'
      }
    },
    shared: {
      deleteBook: 'Da, sterge aceasta carte',
      deleteSure: 'Esti sigur ca vrei sa continui?',
      deleteLost: 'Tot lucrul tau la aceasta carte va fi pierdut!',
      pickTitle: 'Alege un titlu pentru cartea ta',
      chooseCC: 'Alege o licenta CC pentru cartea ta',
      abstract: 'furnizeaza un rezumat pentru cartea ta',
      createBook: 'Creaza cartea',
      bookSummary: 'Un scurt rezumat sau text de prezentare. Nu eobligatoriu. Poate fi omis.',
      bookLicense: 'Aceasta este licenta sub care cartea ta va fi publicata',
      license: 'Lincenta',
      bookTitle: 'Acesta este titlul principal al cartii tale(de exp. "Moby Dick"'
    }
  },
  group: {
    modsSumitted: 'Schimbarile tale au fost trimise proprietarului grupului!',
    submitChanges: 'Trimite schimbarile tale',
    openSyncDialog: 'Deschide dialogul de sincronizare',
    syncPending: 'Sincronizare in asteptare',
    syncFailed: 'Sincronizare esuata',
    syncOk: 'Sincronizare terminata',
    syncPage: 'Sincronizare pagina',
    masterBookUpdated: 'Copia principala a acestei carti a fost actualizata',
    alertBeforeSync: 'Vă rugăm să fiți conștienți de faptul că, în continuare, continutul care nu este încă integrat în copia principală va fi pierdut. Luați în considerare trimiterea înainte de sincronizare.',
    completeSync: 'Sincronizare completa',
    partialSync: 'Sincronizare partiala',
    unsubscribe: 'Dezabonare de la acest grup',
    reviewDiffs: 'Revizuieste diferente',
    diffsMerged: 'Diferente integrate',
    admin: 'Administrator',
    members: 'Membri',
    noMembers: 'Nu sunt membri',
    invited: 'Invitatii',
    addMember: 'Adauga membru',
    noInvitations: 'Nu sunt invitatii in asteptare',
    addBookToGroup: 'Adauga carte la acest grup',
    userDeleted: 'Utilizator sters!',
    userNotFound: 'Utilizator {user} inexistent',
    usersInvited: 'Utilizatori invitați',
    manageRequests: 'Gestionați solicitările',
    inviteAccepted: 'Acum sunteți membru al grupului \'{group}\'!',
    deleteGroup: 'Ștergeți grupul',
    viewGroup: 'Vizualizare grup',
    editGroup: 'Editează grupul',
    viewChanges: 'Vizualizați modificările'
  },
  auth: {
    emailReq: 'Se cere adresa de e-mail',
    pwdReq: 'Parola este necesara',
    emailInvalid: 'Adresa de email invalida',
    pwd6char: 'Parola trebuie sa aiba cel putin 6 caractere',
    newPwdSet: 'Noua ta parola a fost setata',
    repeatPwdReq: 'Este necesata repetarea parolei',
    pwdMatch: 'Parolele trebuie sa coincida',
    resetPwdEmailSent: 'Mesaj de resetare a parolei trimis...',
    pwdUpdated: 'Parola actualizata!'
  }
}
