export const state = () => ({
  pdfIsBuilding: null,
  pdfDocDefinition: {
    pageSize: 'A4',
    pageOrientation: 'portrait',
    pageMargins: [60, 80, 60, 120]
  }
})

export const getters = {
  pdfIsBuilding (state) { return state.pdfIsBuilding },
  pdfDocDefinition (state) { return state.pdfDocDefinition }
}

export const mutations = {
  SET_PDF_IS_BUILDING (state, payload) {
    state.pdfIsBuilding = payload
  }
}
