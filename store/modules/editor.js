export const state = () => ({
  editorClass: null,
  editorWidth: 0,
  drawerWidth: 30,
  columns: [
    {
      width: 0,
      maxWidth: 0,
      id: 'layout',
      drawerLabel: 'Editor',
      content: {
        padding: 30,
        width: 0,
        component: 'BookLayout',
        id: 'bookLayout'
      }
    },
    {
      width: 0,
      maxWidth: 0,
      id: 'preview',
      drawerLabel: 'Preview',
      content: {
        padding: 30,
        width: 0,
        id: 'preview'
      }
    },
    {
      width: 0,
      maxWidth: 0,
      id: 'chaptersAndPages',
      drawerLabel: 'Chapters',
      content: {
        padding: 30,
        width: 0,
        component: 'BookChapters',
        id: 'chaptersAndPages'
      }
    },
    {
      width: 0,
      maxWidth: 0,
      id: 'bookSettings',
      drawerLabel: 'Settings',
      content: {
        padding: 30,
        width: 0,
        component: 'BookSettingsForm',
        id: 'bookSettingsForm'
      }
    }
  ],
  draggedColumnStartWidth: 0,
  prevDraggedColumnStartWidth: 0,
  bookLayout: {
    pageSize: {
      pxTOmm: 0.26458333333719,
      units: 'px',
      lineHeight: 24,
      width: 210,
      height: 297,
      marginTop: 30,
      marginRight: 30,
      marginBottom: 60,
      marginLeft: 30,
      fontSize: 18
    },
    parentHeight: null,
    parentPadding: null,
    colWidth: null,
    colLeft: null,
    colPadding: null
  }
})

export const getters = {
  editorClass (state) { return state.editorClass },
  columns (state) { return state.columns },
  drawerWidth (state) { return state.drawerWidth },
  bookLayout (state) { return state.bookLayout }
}

export const mutations = {
  INIT_COLUMNS (state, payload) {
    state.editorWidth = payload - 1

    state.columns.forEach((column, i) => {
      column.maxWidth = state.editorWidth - (state.drawerWidth * (state.columns.length - 1))

      if (i === 0) {
        column.width = state.editorWidth - (state.drawerWidth * (state.columns.length - 1))
        column.content.width = state.editorWidth - (state.drawerWidth * state.columns.length)
      } else {
        column.width = state.drawerWidth
        column.content.width = 0
      }
    })
  },
  SET_CONTRIBUTOR_COLUMNS (state) {
    state.columns = state.columns.filter(function (col, index, arr) { return col.id !== 'bookSettings' })
  },
  SET_LAYOUT (state, payload) {
    switch (payload.length) {
      case 1:
        state.columns.forEach((column, i) => {
          column.maxWidth = state.editorWidth - (state.drawerWidth * (state.columns.length - 1))

          if (i === payload[0]) {
            column.width = state.editorWidth - (state.drawerWidth * (state.columns.length - 1))
            column.content.width = state.editorWidth - (state.drawerWidth * state.columns.length - 1)
          } else {
            column.width = state.drawerWidth
            column.content.width = 0
          }
        })
        break
      case 2:
        state.columns.forEach((column, i) => {
          column.maxWidth = state.editorWidth - (state.drawerWidth * (state.columns.length - 1))

          if (i === payload[0] || i === payload[1]) {
            column.width = state.editorWidth / 2 - (state.drawerWidth)
            column.content.width = state.editorWidth / 2 - (state.drawerWidth * 2)
          } else {
            column.width = state.drawerWidth
            column.content.width = 0
          }
        })
        break
      case 3:
        break
      case 4:
        break
    }
  },
  EXPAND_COLUMNS (state, payload) {
    state.columns[payload.index].width = state.editorWidth - payload.offset
    state.columns[payload.index].content.width = state.editorWidth - payload.offset - state.drawerWidth
  },
  COLLAPSE_COLUMNS (state, payload) {
    state.columns[payload].width = state.drawerWidth
    state.columns[payload].content.width = 0
  },
  DRAWER_MOUSE_DOWN (state, payload) {
    state.prevDraggedColumnStartWidth = state.columns[payload.index - 1].width
    state.draggedColumnStartWidth = state.columns[payload.index].width
  },
  DRAWER_MOUSE_MOVE (state, payload) {
    if (
      payload.index === 0 ||
      (state.columns[payload.index - 1].width < state.drawerWidth + 1 && Math.sign(payload.deltaX) === -1) ||
      (state.columns[payload.index].width < state.drawerWidth + 1 && Math.sign(payload.deltaX) === 1)
    ) {
      return
    }

    state.columns[payload.index].width = state.draggedColumnStartWidth - payload.deltaX
    state.columns[payload.index].content.width = state.columns[payload.index].width - state.drawerWidth
    state.columns[payload.index - 1].width = state.prevDraggedColumnStartWidth + payload.deltaX
    state.columns[payload.index - 1].content.width = state.columns[payload.index - 1].width - state.drawerWidth
  }
}
