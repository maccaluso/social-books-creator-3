export const state = () => ({
  currentLocaleIndex: 0,
  drawer: false
})

export const getters = {
  currentLocaleIndex (state) { return state.currentLocaleIndex },
  drawer (state) { return state.drawer }
}

export const mutations = {
  TOGGLE_DRAWER (state) {
    state.drawer = !state.drawer
  },
  SYNC_DRAWER (state, payload) {
    state.drawer = payload
  },
  SET_CURRENT_LOCALE_INDEX (state, index) {
    state.currentLocaleIndex = index
  }
}
