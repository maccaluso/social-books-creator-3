import { v4 as uuidv4 } from 'uuid'
import firebaseApp from '~/firebase/app'

export const state = () => ({
  userBooks: [],
  userGroupBooks: [],
  publishedBooks: [],
  newBookTemplate: {
    title: null,
    author: null,
    license: null,
    abstract: null
  },
  availableLicenses: null,
  currentBook: null,
  currentChapterIndex: null,
  currentSubChapterIndex: null,
  diffBook: null,
  lastNewBook: null,
  books: null,
  canUpdatePdf: false
})

export const getters = {
  books (state) { return state.books },
  userBooks (state) { return state.userBooks },
  userGroupBooks (state) { return state.userGroupBooks },
  publishedBooks (state) { return state.publishedBooks },
  newBookTemplate (state) { return state.newBookTemplate },
  availableLicenses (state) { return state.availableLicenses },
  currentBook (state) { return state.currentBook },
  currentChapterIndex (state) { return state.currentChapterIndex },
  currentSubChapterIndex (state) { return state.currentSubChapterIndex },
  diffBook (state) { return state.diffBook },
  lastNewBook (state) { return state.lastNewBook },
  canUpdatePdf (state) { return state.canUpdatePdf }
}

export const actions = {
  getBooks ({ commit }) {
    firebaseApp.firestore().collection('books')
      .onSnapshot(function (querySnapshot) {
        const books = []
        querySnapshot.forEach((book) => {
          const doc = book.data()
          doc.id = book.id
          books.push(doc)
        })
        commit('SET_BOOKS', books)
      })
  },
  async getUserBooks ({ commit, rootState }) {
    await firebaseApp.firestore().collection('books')
      .where('author', '==', rootState.modules.user.user.uid)
      .orderBy('created', 'desc')
      .onSnapshot(function (querySnapshot) {
        const books = []
        querySnapshot.forEach((book) => {
          books.push({ id: book.id, data: book.data() })
        })

        commit('SET_USER_BOOKS', books)
      })
  },
  async getUserGroupBooks ({ commit, rootState }) {
    await firebaseApp.firestore().collection('booksClones')
      .where('author', '==', rootState.modules.user.user.uid)
      .orderBy('created', 'desc')
      .onSnapshot(function (querySnapshot) {
        const books = []
        querySnapshot.forEach(function (book) {
          books.push({ id: book.id, data: book.data() })
        })
        commit('SET_USER_GROUP_BOOKS', books)
      })
  },
  async getPublishedBooks ({ commit, rootState }) {
    await firebaseApp.firestore().collection('books')
      .where('published', '==', true)
      .orderBy('created', 'desc')
      .onSnapshot(function (querySnapshot) {
        const books = []
        querySnapshot.forEach((book) => {
          books.push({ id: book.id, data: book.data() })
        })

        console.log(books)

        commit('SET_PUBLISHED_BOOKS', books)
      })
  },
  async getAvailableLicenses ({ commit }) {
    const licenseCollection = await firebaseApp.firestore().collection('licenses').get()

    const licenses = []
    for (const license of licenseCollection.docs) {
      licenses.push({ id: license.id, data: license.data() })
    }

    commit('SET_AVAILABLE_LICENCES', licenses)
  },
  async createNewBook ({ state, commit }) {
    try {
      await firebaseApp.firestore().collection('books').add({
        abstract: state.newBookTemplate.abstract,
        author: state.newBookTemplate.author,
        chapters: [{
          abstract: 'Chapter 1 abstract',
          children: [],
          content: {
            blocks: [],
            time: Date.now(),
            version: '2.17.0'
          },
          title: 'Chapter 1',
          uuid: uuidv4(),
          hash: Math.floor(Math.random() * 1000000000)
        }],
        format: {
          orientation: 'portrait',
          pageSize: 'A4',
          typography: {
            headings: {
              family: 'Roboto',
              size: 'medium'
            },
            body: {
              family: 'Roboto',
              size: 'medium'
            },
            pre: {
              family: 'Inconsolata',
              size: 'medium'
            }
          }
        },
        cover: {
          showTitle: true,
          titleBackgroundColor: '#ffffff',
          titleColor: '#42ab3d',
          titlePosition: 'centerBottom'
        },
        license: state.newBookTemplate.license,
        title: state.newBookTemplate.title,
        created: Date.now(),
        updated: Date.now()
      }).then(function (docRef) {
        commit('SET_CURRENT_NEW_BOOK', docRef.id)
      })

      this.$toast.global.sbc_success({ message: 'New book created...' })
    } catch (error) {
      this.$toast.global.sbc_error({ message: error.message })
    }
  },
  async getCurrentBook ({ state, commit }, payload) {
    await firebaseApp.firestore().collection('books').doc(payload.id).onSnapshot(function (doc) {
      const book = doc
      const currentContent = state.currentChapterIndex ? state.currentChapterIndex : 0
      const currentSubContent = typeof state.currentSubChapterIndex !== 'undefined' ? state.currentSubChapterIndex : undefined
      commit('SET_CURRENT_BOOK', { id: book.id, data: book.data() })
      commit('SET_CURRENT_CONTENT', [currentContent, currentSubContent])
    }, function () {
      this.$toast.global.sbc_warning({ message: 'This book doesn\'t exists' })
    })
  },
  async deleteBookByID ({ dispatch }, payload) {
    await firebaseApp.firestore().collection('books').doc(payload).delete()
    dispatch('getUserBooks')
  },
  async duplicateBook ({ dispatch }, payload) {
    const book = payload.book.data
    const withContent = payload.content
    book.title = withContent ? book.title + ' (Copy)' : book.title + ' (Template)'
    book.created = Date.now()
    book.updated = Date.now()
    book.chapters.forEach((chapter) => {
      chapter.uuid = uuidv4()
      chapter.hash = Math.floor(Math.random() * 1000000000)
      if (!withContent) {
        chapter.content = {}
      }
      chapter.children.forEach((child) => {
        child.uuid = uuidv4()
        child.hash = Math.floor(Math.random() * 1000000000)
        if (!withContent) {
          child.content = {}
        }
      })
    })
    try {
      await firebaseApp.firestore().collection('books').add(book)

      this.$toast.global.sbc_success({ message: 'New book created...' })
    } catch (error) {
      this.$toast.global.sbc_error({ message: error.message })
    }
  },
  // batchDelete () {
  //   // console.log('batchDelete')
  //   firebaseApp.firestore().collection('books')
  //     .where('title', '==', 'Educational technology in ELT. Benefits and outcomes').get()
  //     .then(function (querySnapshot) {
  //       // Once we get the results, begin a batch
  //       // const batch = firebaseApp.firestore().batch()

  //       querySnapshot.forEach(function (doc) {
  //         // For each doc, add a delete operation to the batch
  //         // batch.delete(doc.ref)
  //       })

  //       // Commit the batch
  //       // return batch.commit()
  //     }).then(function () {
  //       // Delete completed!
  //       // ...
  //     })
  // },
  saveBookSettings ({ state }) {
    return firebaseApp.firestore().collection('books').doc(state.currentBook.id).update({
      title: state.currentBook.data.title,
      abstract: state.currentBook.data.abstract,
      published: state.currentBook.data.published || false,
      format: state.currentBook.data.format || false,
      license: state.currentBook.data.license,
      updated: Date.now()
    })
  },
  updateChapters ({ state }) {
    return firebaseApp.firestore().collection('books').doc(state.currentBook.id).update({
      chapters: state.currentBook.data.chapters,
      title: state.currentBook.data.title,
      abstract: state.currentBook.data.abstract,
      published: state.currentBook.data.published || false,
      format: state.currentBook.data.format || false,
      license: state.currentBook.data.license,
      cover: state.currentBook.data.cover,
      updated: Date.now()
    })
  },
  updateChaptersContributor ({ state }) {
    return firebaseApp.firestore().collection('booksClones').doc(state.currentBook.id).update({
      chapters: state.currentBook.data.chapters,
      cover: state.currentBook.data.cover,
      updated: Date.now()
    })
  },
  mergeDiffs ({ state }) {
    return firebaseApp.firestore().collection('books').doc(state.diffBook.id).get().then(function (doc) {
      const book = doc.data()
      state.diffBook.data.chapters.forEach((chapter) => {
        const index = book.chapters.findIndex(chap => chap.uuid === chapter.uuid)
        book.chapters[index].content = chapter.content
        book.chapters[index].updated = Date.now()
      })
      doc.ref.update({
        chapters: book.chapters,
        updated: Date.now()
      })
    })
    // return firebaseApp.firestore().collection('books').doc(state.diffBook.id).update({
    //   chapters: state.diffBook.data.chapters,
    //   updated: Date.now()
    // })
  },
  setDiffBook  ({ commit }, payload) {
    return firebaseApp.firestore().collection('booksClones').doc(payload.id).onSnapshot(function (doc) {
      const diffBook = payload.book
      const cloneBook = doc.data()
      diffBook.data.chapters.forEach((chapter) => {
        const index = cloneBook.chapters.findIndex(chap => chap.uuid === chapter.uuid)
        if (cloneBook.chapters[index] && cloneBook.chapters[index].submitted) {
          chapter.clone = cloneBook.chapters[index]
          chapter.clone.bookId = doc.id
        }
        chapter.children.forEach((child) => {
          const index2 = cloneBook.chapters[index].children.findIndex(chap => chap.uuid === child.uuid)
          if (cloneBook.chapters[index].children[index2] && cloneBook.chapters[index].children[index2].submitted) {
            child.clone = cloneBook.chapters[index].children[index2]
            child.clone.bookId = doc.id
          }
        })
      })
      commit('SET_DIFF_BOOK', diffBook)
    })
  },
  setDiffBookForPartialSync  ({ commit }, payload) {
    return firebaseApp.firestore().collection('books').doc(payload.id).onSnapshot(function (doc) {
      const diffBook = { id: doc.id, data: doc.data() }
      const cloneBook = payload.book.data
      let partials = 0
      diffBook.data.chapters.forEach((chapter) => {
        const index = cloneBook.chapters.findIndex(chap => chap.uuid === chapter.uuid)
        if (index !== -1 && cloneBook.chapters[index].hash !== chapter.hash) {
          chapter.clone = cloneBook.chapters[index]
          partials = ++partials
        } else if (index === -1) {
          chapter.clone = chapter
          chapter.isNew = true
        }
        chapter.children.forEach((child) => {
          const index2 = cloneBook.chapters[index].children.findIndex(chap => chap.uuid === child.uuid)
          if (index2 !== -1 && cloneBook.chapters[index].children[index2].hash !== child.hash) {
            child.clone = cloneBook.chapters[index].children[index2]
            partials = ++partials
          } else if (index2 === -1) {
            child.clone = child
            child.isNew = true
          }
        })
      })
      diffBook.data.partials = partials
      commit('SET_DIFF_BOOK', diffBook)
    })
  },
  getCurrentBookClone ({ commit }, payload) {
    console.log('get current clone book')
    return firebaseApp.firestore().collection('booksClones').doc(payload.id).onSnapshot(function (doc) {
      const book = doc
      const currentContent = state.currentChapterIndex ? state.currentChapterIndex : 0
      const currentSubContent = typeof state.currentSubChapterIndex !== 'undefined' ? state.currentSubChapterIndex : undefined
      commit('SET_CURRENT_BOOK', { id: book.id, data: book.data() })
      commit('SET_CURRENT_CONTENT', [currentContent, currentSubContent])
    }, function () {
      this.$toast.global.sbc_warning({ message: 'This book doesn\'t exists' })
    })
  },
  syncBook ({ commit }, payload) {
    const cloneBook = payload.book
    const cloneBook2 = JSON.parse(JSON.stringify(payload.book))
    const full = payload.full
    const selected = payload.selected ? payload.selected : []
    firebaseApp.firestore().collection('books').doc(payload.book.data.originalId).get().then(function (doc) {
      const book = doc.data()

      cloneBook.data.chapters = book.chapters
      cloneBook.data.chapters.forEach((chapter) => {
        chapter.edited = false
        chapter.submitted = false
        chapter.children.forEach((child) => {
          child.edited = false
          child.submitted = false
        })
      })
      if (full) {
        cloneBook.data.needSync = false
      } else {
        book.chapters.forEach((chapter) => {
          const index = cloneBook.data.chapters.findIndex(chap => chap.uuid === chapter.uuid)
          const index2 = cloneBook2.data.chapters.findIndex(chap => chap.uuid === chapter.uuid)
          if (!selected.includes(chapter.uuid)) {
            console.log(index, index2)
            if (index !== -1 && index2 !== -1) {
              cloneBook.data.chapters[index].content = cloneBook2.data.chapters[index2].content
              cloneBook.data.chapters[index].abstract = cloneBook2.data.chapters[index2].abstract
              cloneBook.data.chapters[index].title = cloneBook2.data.chapters[index2].title
              cloneBook.data.chapters[index].edited = cloneBook2.data.chapters[index2].edited
              cloneBook.data.chapters[index].submitted = cloneBook2.data.chapters[index2].submitted
              cloneBook.data.chapters[index].hash = cloneBook2.data.chapters[index2].hash
            }
          }
          chapter.children.forEach((child) => {
            const indexChild = cloneBook.data.chapters[index].children.findIndex(chap => chap.uuid === child.uuid)
            const indexChild2 = cloneBook2.data.chapters[index2].children.findIndex(chap => chap.uuid === child.uuid)

            if (!selected.includes(child.uuid)) {
              if (indexChild !== -1 && indexChild2 !== -1) {
                cloneBook.data.chapters[index].children[indexChild].content = cloneBook2.data.chapters[index2].children[indexChild2].content
                cloneBook.data.chapters[index].children[indexChild].abstract = cloneBook2.data.chapters[index2].children[indexChild2].abstract
                cloneBook.data.chapters[index].children[indexChild].title = cloneBook2.data.chapters[index2].children[indexChild2].title
                cloneBook.data.chapters[index].children[indexChild].edited = cloneBook2.data.chapters[index2].children[indexChild2].edited
                cloneBook.data.chapters[index].children[indexChild].submitted = cloneBook2.data.chapters[index2].children[indexChild2].submitted
                cloneBook.data.chapters[index].children[indexChild].hash = cloneBook2.data.chapters[index2].children[indexChild2].hash
              }
            }
          })
        })
      }

      cloneBook.data.updated = Date.now()
      firebaseApp.firestore().collection('booksClones').doc(cloneBook.id).update(cloneBook.data)
    })
  },
  updateBookPDF ({ state }, payload) {
    // return firebaseApp.firestore().collection('books').doc(state.currentBook.id).update({
    let path = 'books'
    if (payload.book.data.originalId) {
      path = 'booksClones'
    }
    return firebaseApp.firestore().collection(path).doc(payload.book.id).update({
      pdfURL: payload.url,
      pdfFileName: payload.name,
      pdfPages: payload.pages
    })
  }
}

export const mutations = {
  SET_BOOKS (state, books) { state.books = books },
  SET_USER_BOOKS (state, books) { state.userBooks = books },
  SET_USER_GROUP_BOOKS (state, books) { state.userGroupBooks = books },
  SET_PUBLISHED_BOOKS (state, books) { state.publishedBooks = books },
  SET_NEW_BOOK_AUTHOR (state, payload) { state.newBookTemplate.author = payload },
  SET_AVAILABLE_LICENCES (state, licenses) { state.availableLicenses = licenses },
  RESET_NEW_BOOK_DATA (state) {
    state.newBookTemplate = {
      title: null,
      license: null,
      abstract: null,
      cover: null
    }
  },
  SET_CURRENT_BOOK (state, payload) {
    state.currentBook = payload
  },
  SET_CURRENT_CONTENT (state, payload) {
    state.currentChapterIndex = payload[0]
    state.currentSubChapterIndex = payload[1]
  },
  SYNC_CHAPTERS_CONTENT (state, payload) {
    if (state.currentSubChapterIndex !== undefined) {
      state.currentBook.data.chapters[state.currentChapterIndex].children[state.currentSubChapterIndex].content = payload.outputData
      if (!payload.isOwner) {
        state.currentBook.data.chapters[state.currentChapterIndex].children[state.currentSubChapterIndex].edited = true
      }
      state.currentBook.data.chapters[state.currentChapterIndex].children[state.currentSubChapterIndex].hash = Math.floor(Math.random() * 1000000000)
    } else {
      state.currentBook.data.chapters[state.currentChapterIndex].content = payload.outputData
      if (!payload.isOwner) {
        state.currentBook.data.chapters[state.currentChapterIndex].edited = true
      }
      state.currentBook.data.chapters[state.currentChapterIndex].hash = Math.floor(Math.random() * 1000000000)
    }
  },
  ADD_CHAPTER (state) {
    state.currentBook.data.chapters.push({
      uuid: uuidv4(),
      hash: Math.floor(Math.random() * 1000000000),
      abstract: 'An abstract',
      children: [],
      content: {},
      title: 'New Chapter'
    })
  },
  DELETE_CHAPTER (state, payload) {
    state.currentBook.data.chapters.splice(payload, 1)
  },
  UPDATE_CHAPTER_DATA (state, payload) {
    state.currentBook.data.chapters[state.currentChapterIndex].title = payload.title
    state.currentBook.data.chapters[state.currentChapterIndex].abstract = payload.abstract
  },
  ADD_SUB_CHAPTER (state, payload) {
    state.currentBook.data.chapters[payload].children.push({
      uuid: uuidv4(),
      hash: Math.floor(Math.random() * 1000000000),
      abstract: 'A subchapter abstract',
      content: {},
      title: 'New Subchapter'
    })
  },
  DELETE_SUB_CHAPTER (state, payload) {
    state.currentBook.data.chapters[payload.index].children.splice(payload.subIndex, 1)
  },
  UPDATE_SUB_CHAPTER_DATA (state, payload) {
    state.currentBook.data.chapters[state.currentChapterIndex].children[state.currentSubChapterIndex].title = payload.title
    state.currentBook.data.chapters[state.currentChapterIndex].children[state.currentSubChapterIndex].abstract = payload.abstract
  },
  MOVE_CHAPTER_DOWN (state, payload) {
    state.currentBook.data.chapters.splice(payload.index + 1, 0, state.currentBook.data.chapters.splice(payload.index, 1)[0])
  },
  MOVE_CHAPTER_UP (state, payload) {
    state.currentBook.data.chapters.splice(payload.index - 1, 0, state.currentBook.data.chapters.splice(payload.index, 1)[0])
  },
  MOVE_SUB_CHAPTER_DOWN (state, payload) {
    state.currentBook.data.chapters[payload.index].children.splice(payload.subIndex + 1, 0, state.currentBook.data.chapters[payload.index].children.splice(payload.subIndex, 1)[0])
  },
  MOVE_SUB_CHAPTER_UP (state, payload) {
    state.currentBook.data.chapters[payload.index].children.splice(payload.subIndex - 1, 0, state.currentBook.data.chapters[payload.index].children.splice(payload.subIndex, 1)[0])
  },
  CHANGES_SUBMITTED (state, payload) {
    state.currentBook.data.chapters.forEach((chapter) => {
      if (chapter.uuid === payload[0]) {
        chapter.submitted = true
        if (payload.length > 1) {
          chapter.children.forEach((child) => {
            if (child.uuid === payload[1]) {
              child.submitted = true
            }
          })
        }
      }
    })
  },
  SET_DIFF_BOOK (state, payload) {
    state.diffBook = payload
  },
  ACCEPT_CHAPTER_CHANGE (state, payload) {
    if (payload[1] === 'chapter') {
      const content = payload[2].content
      const index = state.diffBook.data.chapters.findIndex(chapter => chapter.uuid === payload[2].uuid)
      if (payload[0]) {
        state.diffBook.data.chapters[index].content = content
        state.diffBook.data.chapters[index].time = Date.now()
      }
      delete state.diffBook.data.chapters[index].clone
    }
    if (payload[1] === 'subchapter') {
      const content = payload[2].content
      const chapIndex = state.diffBook.data.chapters.findIndex(chapter => chapter.uuid === payload[3])
      const index = state.diffBook.data.chapters[chapIndex].children.findIndex(child => child.uuid === payload[2].uuid)
      if (payload[0]) {
        state.diffBook.data.chapters[chapIndex].children[index].content = content
        state.diffBook.data.chapters[chapIndex].children[index].time = Date.now()
      }
      delete state.diffBook.data.chapters[chapIndex].children[index].clone
    }
  },
  SET_CURRENT_NEW_BOOK (state, payload) {
    state.lastNewBook = payload
  },
  CAN_UPDATE_PDF (state, payload) {
    state.canUpdatePdf = payload
  }
}
