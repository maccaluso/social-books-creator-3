import firebase from 'firebase/app'
import firebaseApp from '~/firebase/app'

export const state = () => ({
  newGroupOwner: null,
  newGroupName: null,
  newGroupInvited: [],
  newGroupMembers: [],
  newGroupSharedBooks: [],
  userGroups: [],
  userGroupsLoaded: false,
  group: null,
  groupMeta: null,
  groupBooks: null,
  pendingInvitations: null,
  groupRequests: [],
  syncRequests: null
})

export const getters = {
  newGroupOwner (state) { return state.newGroupOwner },
  newGroupName (state) { return state.newGroupName },
  newGroupMembers (state) { return state.newGroupMembers },
  newGroupInvited (state) { return state.newGroupInvited },
  newGroupSharedBooks (state) { return state.newGroupSharedBooks },
  userGroups (state) { return state.userGroups },
  userGroupsLoaded (state) { return state.userGroupsLoaded },
  group (state) { return state.group },
  groupMeta (state) { return state.groupMeta },
  groupBooks (state) { return state.groupBooks },
  pendingInvitations (state) { return state.pendingInvitations },
  groupRequests (state) { return state.groupRequests },
  syncRequests (state) { return state.syncRequests }
}

export const actions = {
  createNewGroup ({ state }, payload) {
    return firebaseApp.firestore().collection('groups').add({
      owner: state.newGroupOwner,
      name: state.newGroupName,
      invited: payload.users,
      members: state.newGroupMembers,
      books: state.newGroupSharedBooks
    })
  },
  getUserGroups ({ commit }, uid) {
    firebaseApp.firestore().collection('groups')
      .where('members', 'array-contains', uid)
      .onSnapshot(function (querySnapshot) {
        const groupArray = []
        querySnapshot.forEach(function (group) {
          groupArray.push({
            id: group.id,
            data: group.data()
          })
        })
        commit('SET_USER_GROUPS', groupArray)
        commit('SET_USER_GROUPS_LOADED')
      })
  },
  deleteGroup ({ commit, dispatch }, payload) {
    return firebaseApp.firestore().doc('/groups/' + payload.item.id).delete().then(() => {
      dispatch('deleteBookClone', [payload.item.id])
    })
  },
  deleteUserGroup ({ commit, dispatch }, payload) {
    return firebaseApp.firestore().collection('groups')
      .doc(payload.item.id).get().then(function (doc) {
        const group = doc.data()
        let userDeleted = false
        group.members.forEach((user, index) => {
          if (user === payload.uid) {
            group.members.splice(index, 1)
            userDeleted = true
          }
        })

        if (!userDeleted) {
          group.invited.forEach((user, index) => {
            if (user === payload.uid) {
              group.invited.splice(index, 1)
              userDeleted = true
            }
          })
        } else {
          dispatch('deleteBookClone', [payload.item.id, payload.uid])
        }

        firebaseApp.firestore().collection('groups')
          .doc(payload.item.id).update(group)
      })
  },
  deleteBookClone ({ commit }, payload) {
    if (payload.length > 1) {
      firebaseApp.firestore().collection('booksClones')
        .where('groupId', '==', payload[0]).where('author', '==', payload[1])
        .get().then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            doc.ref.delete()
          })
        })
    } else {
      firebaseApp.firestore().collection('booksClones')
        .where('groupId', '==', payload[0])
        .get().then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            doc.ref.delete()
          })
        })
    }
  },
  getPendingInvitations ({ commit }, payload) {
    firebaseApp.firestore().collection('groups')
      .where('invited', 'array-contains', payload)
      .onSnapshot(function (querySnapshot) {
        const groupArray = []
        querySnapshot.forEach(function (group) {
          groupArray.push({
            id: group.id,
            data: group.data()
          })
        })

        commit('SET_USER_INVITATIONS', groupArray)
      })
  },
  getSyncRequests ({ commit }, payload) {
    firebaseApp.firestore().collection('booksClones')
      .where('author', '==', payload)
      .where('needSync', '==', true)
      .onSnapshot(function (querySnapshot) {
        const hasSyncRequests = querySnapshot.size

        commit('SET_SYNC_REQUESTS_NUMBER', hasSyncRequests)
      })
  },
  updateUserInvitation ({ commit }, payload) {
    firebaseApp.firestore().collection('groups')
      .doc(payload.invId).get().then(function (doc) {
        // console.log(doc.id, '=>', doc.data())
        const group = doc.data()
        group.invited.forEach((user, index) => {
          if (user === payload.uid) {
            group.invited.splice(index, 1)
          }
        })

        if (payload.accepted) {
          group.members.push(payload.uid)
        }

        firebaseApp.firestore().collection('groups')
          .doc(payload.invId).update(group)
      })
  },
  getGroupMeta ({ commit }, payload) {
    firebaseApp.firestore().collection('groups')
      .doc(payload.id).onSnapshot(function (snapshot) {
        if (!snapshot.exists) {
          console.log('book not found')
        } else {
          const groupId = snapshot.id
          const group = snapshot.data()
          commit('SET_GROUP', { id: groupId, group })
          const promises = []
          group.members.forEach((member, index) => {
            promises.push(firebaseApp.firestore().collection('users').doc(member).get())
          })

          promises.push(firebaseApp.firestore().collection('users').doc(group.owner).get())

          group.invited.forEach((invited, index) => {
            promises.push(firebaseApp.firestore().collection('users').doc(invited).get())
          })

          const promisesBooks = []
          const booksClones = []

          if (group.owner === payload.uid) {
            group.books.forEach((book) => {
              promisesBooks.push(firebaseApp.firestore().collection('books').doc(book).get())
            })
          } else {
            let countBooks = 0
            group.books.forEach((book) => {
              firebaseApp.firestore().collection('booksClones')
                .where('originalId', '==', book).where('author', '==', payload.uid).where('groupId', '==', payload.id)
                .get().then(function (querySnapshot) {
                  querySnapshot.forEach((doc) => {
                    booksClones.push({ id: doc.id, data: doc.data() })
                    countBooks = ++countBooks
                    if (countBooks === group.books.length) {
                      commit('SET_GROUP_BOOKS', booksClones)
                    }
                  })
                })
            })
          }

          // Wait for all promises to resolve
          Promise.all(promises).then(function (res) {
            const groupMeta = {
              members: [],
              invited: [],
              owner: null
            }
            res.forEach((r) => {
              // console.log(r.id, r.data())
              if (r.id === group.owner) {
                const id = r.id
                const data = r.data()
                groupMeta.owner = { id, data }
              }
              if (group.members.includes(r.id) && r.id !== group.owner) {
                const id = r.id
                const data = r.data()
                groupMeta.members.push({ id, data })
              }
              if (group.invited.includes(r.id)) {
                const id = r.id
                const data = r.data()
                groupMeta.invited.push({ id, data })
              }
            })
            // console.log(groupMeta)
            commit('SET_GROUP_META', groupMeta)

            if (promisesBooks.length) {
              Promise.all(promisesBooks).then(function (res) {
                const groupBooks = []
                res.forEach((r) => {
                  const id = r.id
                  const data = r.data()
                  groupBooks.push({ id, data })
                })
                commit('SET_GROUP_BOOKS', groupBooks)
              }).catch(function (error) {
                console.log('Error', error)
              })
            }
          })
        }
      })
  },
  addInvitedUsers ({ commit }, payload) {
    return firebaseApp.firestore().collection('groups')
      .doc(payload.id).get().then(function (group) {
        const groupData = group.data()
        const invitationMerge = groupData.invited.concat(payload.users)
        firebaseApp.firestore().collection('groups')
          .doc(payload.id).update({ invited: invitationMerge })
      })
  },
  addBooks ({ commit }, payload) {
    return firebaseApp.firestore().collection('groups')
      .doc(payload.id).get().then(function (group) {
        const groupData = group.data()
        const booksMerge = groupData.books.concat(payload.books)
        group.ref.update({ books: booksMerge })
      })
  },
  setBooksClones ({ commit }, payload) {
    // console.log(payload)
    firebaseApp.firestore().collection('groups')
      .doc(payload.groupId).get().then(function (group) {
        const groupData = group.data()
        groupData.books.forEach((book) => {
          firebaseApp.firestore().collection('books')
            .doc(book).get().then(function (book) {
              const bookClone = book.data()
              bookClone.originalId = book.id
              bookClone.groupId = payload.groupId
              bookClone.originalAuthor = bookClone.author
              bookClone.author = payload.uid
              bookClone.chapters.forEach((chapter) => {
                chapter.submitted = false
                chapter.edited = false

                chapter.children.forEach((child) => {
                  child.submitted = false
                  child.edited = false
                })
              })
              firebaseApp.firestore().collection('booksClones').add(bookClone)
            })
        })
      })
  },
  submitChangesToOwner ({ state }, payload) {
    return firebaseApp.firestore().collection('groups').doc(payload.book.data.groupId)
      .update({
        requests: firebase.firestore.FieldValue.arrayUnion({
          user: payload.book.data.author,
          bookClone: payload.book.id,
          originalBook: payload.book.data.originalId,
          chapters: payload.chapter
        })
      })
  },
  resetRequests ({ state }, payload) {
    firebaseApp.firestore().collection('groups').doc(payload.group).get().then(function (doc) {
      const group = doc.data()
      group.requests.slice().reverse().forEach(function (req, index, object) {
        if (req.user === payload.user) {
          group.requests.splice(object.length - 1 - index, 1)
        }
      })
      doc.ref.update({
        requests: group.requests
      })
    })
  },
  getChangeRequests ({ commit, state }, payload) {
    const requests = []
    if (state.group.group.requests) {
      state.group.group.requests.forEach((request) => {
        if (!requests.some(el => el.bookClone === request.bookClone)) {
          const bookClone = request.bookClone
          const chapters = request.chapters
          const originalBook = request.originalBook
          const bookIndex = payload.books.findIndex(book => book.id === request.originalBook)
          const bookName = payload.books[bookIndex].data.title
          const req = {
            bookClone,
            originalBook,
            bookName,
            chapters: [
              chapters
            ]
          }
          const index = payload.users.findIndex(x => x.id === request.user)
          req.user = payload.users[index]

          // const indexBook = payload.groupBooks.findIndex(x => x.id === request.originalBook)
          // req.originalBook = payload.originalBook[indexBook]

          requests.push(req)
        } else {
          const index = requests.findIndex(x => x.bookClone === request.bookClone)
          requests[index].chapters.push(request.chapters)
        }
      })
    }

    commit('SET_GROUP_REQUESTS', requests)
  },
  setSyncNeeded ({ commit }, payload) {
    // console.log(payload)
    firebaseApp.firestore().collection('booksClones')
      .where('groupId', '==', payload.groupId).where('originalId', '==', payload.bookId).get().then(function (snapshot) {
        const promises = []
        snapshot.forEach((doc) => {
          promises.push(doc.ref.update({
            needSync: true
          }))
        })
        return Promise.all(promises)
      })
  },
  setSyncNeededFromBook ({ commit }, payload) {
    // console.log(payload)
    firebaseApp.firestore().collection('booksClones')
      .where('originalId', '==', payload).get().then(function (snapshot) {
        const promises = []
        snapshot.forEach((doc) => {
          promises.push(doc.ref.update({
            needSync: true
          }))
        })
        return Promise.all(promises)
      })
  },
  sendSyncPushNotifications ({ state }, payload) {
    console.log(state)
    state.group.group.members.forEach((member) => {
      const index = payload.users.findIndex(u => u.id === member)
      const user = payload.users[index]
      if (user.data.fcmToken && user.id !== state.group.group.owner) {
        console.log('push')

        const key = 'AAAAD918uoc:APA91bHwoendnBNTVi15fT_gUWnfxbowhumVikDyLqodj47rC86ep0cfVax11zyiEaN7y2cJN-aTVJPKDvuCOKkTVbVEBlB9oJ3BuGEVpuMVP8ngOh79ausTKsojAAo6kviJkWmnvEuy'

        const notification = {
          title: 'Async notification',
          body: 'Ciao ' + user.data.email + '! One of you books needs to be synced',
          icon: '/favicon-32x32.png',
          click_action: 'http://localhost:3000/app/book-shelf'
        }

        fetch('https://fcm.googleapis.com/fcm/send', {
          method: 'POST',
          headers: {
            Authorization: 'key=' + key,
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            notification,
            to: user.data.fcmToken
          })
        }).then(function (response) {
          // console.log(response)
        }).catch(function (error) {
          console.error(error)
        })
      }
    })
  }
}

export const mutations = {
  RESET_NEW_GROUP_DATA (state) {
    state.newGroupOwner = null
    state.newGroupName = null
    state.newGroupInvited = []
    state.newGroupMembers = []
    state.newGroupSharedBooks = []
  },
  SET_USER_GROUPS (state, payload) {
    state.userGroups = payload
  },
  SET_USER_GROUPS_LOADED (state) {
    state.userGroupsLoaded = true
  },
  SET_GROUP_OWNER (state, payload) {
    state.newGroupMembers = [payload]
    state.newGroupOwner = payload
  },
  SET_USER_INVITATIONS (state, payload) {
    state.pendingInvitations = payload
  },
  SET_GROUP (state, payload) {
    state.group = payload
  },
  SET_GROUP_META (state, payload) {
    state.groupMeta = payload
  },
  SET_GROUP_BOOKS (state, payload) {
    state.groupBooks = payload
  },
  SET_GROUP_REQUESTS (state, payload) {
    state.groupRequests = payload
  },
  SET_SYNC_REQUESTS_NUMBER (state, payload) {
    state.syncRequests = payload
  }
}
