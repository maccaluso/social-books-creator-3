import Cookies from 'js-cookie'
import { auth } from 'firebase'
import firebaseApp from '~/firebase/app'

export const state = () => ({
  uid: null,
  user: null,
  userMeta: null,
  users: ['foo', 'bar', 'fizz', 'buzz'],
  mailFrequency: [
    {
      text: 'Every week',
      value: 'week'
    },
    {
      text: 'Every two weeks',
      value: 'weeks2'
    },
    {
      text: 'Every month',
      value: 'month'
    }
  ],
  isSavingPersonalData: false,
  isSavingOrganization: false,
  notifications: null
})

export const getters = {
  uid (state) { if (state.user && state.user.uid) { return state.user.uid } else { return null } },
  user (state) { return state.user },
  userMeta (state) { return state.userMeta },
  users (state) { return state.users },
  activeTab (state) { return state.activeTab },
  navItems (state) { return state.navItems },
  adminNavItems (state) { return state.adminNavItems },
  isAuthenticated (state) { return !!state.user && !!state.user.uid },
  isAdmin (state) { return !!state.userMeta && state.userMeta.role === 'admin' },
  isSavingPersonalData (state) { return state.isSavingPersonalData },
  isSavingOrganization (state) { return state.isSavingOrganization },
  mailFrequency (state) { return state.mailFrequency },
  notifications (state) { return state.notifications }
}

export const actions = {
  async login ({ dispatch }, user) {
    const token = await firebaseApp.auth().currentUser.getIdToken(true)

    const userInfo = {
      displayName: user.displayName,
      email: user.email,
      emailVerified: user.emailVerified,
      photoURL: user.photoURL,
      uid: user.uid
    }

    Cookies.set('access_token', token)

    await dispatch('setUSER', userInfo)
    await dispatch('setUSERMETA', userInfo.uid)
    await dispatch('saveUID', userInfo.uid)
    await dispatch('modules/groups/getPendingInvitations', userInfo.uid, { root: true })
    await dispatch('modules/groups/getSyncRequests', userInfo.uid, { root: true })
  },

  async logout ({ commit }) {
    await firebaseApp.auth().signOut()

    Cookies.remove('access_token')
    commit('setUSER', null)
    commit('saveUID', null)
  },

  sendVerificationEmail () {
    return firebaseApp.auth().currentUser.sendEmailVerification()
  },

  verifyEmail (state, payload) {
    return firebaseApp.auth().applyActionCode(payload)
  },

  resetPassword (state, payload) {
    return firebaseApp.auth().sendPasswordResetEmail(payload)
  },

  verifyPasswordResetCode (state, payload) {
    return firebaseApp.auth().verifyPasswordResetCode(payload)
  },

  confirmPasswordReset (state, payload) {
    return firebaseApp.auth().confirmPasswordReset(payload.actionCode, payload.newPassword)
  },

  verifyOldPassword (state, payload) {
    let credentials = {}
    credentials = auth.EmailAuthProvider.credential(
      firebaseApp.auth().currentUser.email,
      payload.oldPassword
    )
    return firebaseApp.auth().currentUser.reauthenticateWithCredential(credentials)
  },

  changePassword (state, payload) {
    return firebaseApp.auth().currentUser.updatePassword(payload.newPassword)
  },

  linkUserMeta (state, payload) {
    return firebaseApp.firestore().collection('users').doc(payload.userId).set({
      address: null,
      avatar: null,
      bio: null,
      email: payload.email,
      firstName: null,
      gradient: 'linear-gradient(45deg, #B6C1D4, #EC68B1)',
      role: 'basic',
      organization: null,
      surname: null,
      fcmToken: null,
      userSettings: {
        hints: null,
        mailFrequency: null,
        mailFromUs: null
      }
    })
  },

  async setUserDefaultLang ({ state }, payload) {
    await firebaseApp.firestore().collection('users').doc(state.user.uid).update({ userLang: payload })
  },

  async setUserAvatar ({ state }, payload) {
    await firebaseApp.firestore().collection('users').doc(state.user.uid).update({ avatar: payload })
  },

  async updateFcmToken ({ state }, payload) {
    await firebaseApp.firestore().collection('users').doc(state.user.uid).update({ fcmToken: payload })
  },

  async unSetUserAvatar ({ state }) {
    await firebaseApp.firestore().collection('users').doc(state.user.uid).update({ avatar: null })
  },

  async updateUserMeta ({ state, commit }) {
    await firebaseApp.firestore().collection('users').doc(state.user.uid).update(state.userMeta)
    commit('SET_IS_SAVING_PERSONAL_DATA', false)
  },

  async updateUserMetaOrganization ({ state, commit }) {
    await firebaseApp.firestore().collection('users').doc(state.user.uid).update(state.userMeta)
    commit('SET_IS_SAVING_ORGANIZATION', false)
  },

  sendInAppNotification ({ commit }, payload) {
    payload.viewed = false
    payload.time = Date.now()
    firebaseApp.firestore().collection('notifications').add(payload)
  },

  getInAppNotifications ({ commit }, payload) {
    firebaseApp.firestore().collection('notifications').where('user', '==', payload).orderBy('time', 'desc').onSnapshot(function (querySnapshot) {
      const notifications = []
      querySnapshot.forEach(function (notification) {
        notifications.push({
          id: notification.id,
          data: notification.data()
        })
      })
      commit('SET_USER_NOTIFICATIONS', notifications)
    })
  },

  setInAppNotificationViewed ({ commit }, payload) {
    firebaseApp.firestore().collection('notifications').doc(payload).update({
      viewed: true
    })
  },

  saveUID ({ commit }, uid) {
    commit('saveUID', uid)
  },

  setUSER ({ commit }, user) {
    commit('setUSER', user)
  },

  setUSERMETA ({ commit }, uid) {
    firebaseApp.firestore().collection('users').doc(uid).onSnapshot(function (doc) {
      // console.log(doc.data().userSettings)
      const userMeta = doc.data()
      if (!userMeta.userSettings) { userMeta.userSettings = { hints: true, mailFrequency: 'week', mailFromUs: true } }
      if (!userMeta.userLang) { userMeta.userLang = { index: 0, locale: 'English' } }
      commit('setUSERMETA', userMeta)
    })
  },

  getUsers ({ commit }) {
    firebaseApp.firestore().collection('users')
      .onSnapshot(function (querySnapshot) {
        const usersArray = []
        querySnapshot.forEach(function (user) {
          usersArray.push({
            id: user.id,
            data: user.data()
          })
        })
        commit('SET_USERS', usersArray)
      })
  }
}

export const mutations = {
  saveUID (state, uid) {
    state.uid = uid
  },
  setUSER (state, user) {
    state.user = user
  },
  setUSERMETA (state, userMeta) {
    state.userMeta = userMeta
  },
  SET_USERS (state, usersList) {
    state.users = usersList
  },
  SET_ACTIVE_TAB (state, tabIndex) {
    state.activeTab = tabIndex
  },
  SET_IS_SAVING_PERSONAL_DATA (state, isSaving) {
    state.isSavingPersonalData = isSaving
  },
  SET_IS_SAVING_ORGANIZATION (state, isSaving) {
    state.isSavingOrganization = isSaving
  },
  SET_EMAIL_VERIFIED (state) {
    state.user.emailVerified = true
  },
  SET_USER_NOTIFICATIONS (state, notifications) {
    state.notifications = notifications
  }
}
