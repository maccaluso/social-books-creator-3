import { getField, updateField } from 'vuex-map-fields'
import { getUserFromCookie, getUserFromBrowserCookie } from '@/helpers'

export const state = () => ({
  counter: 0
})

export const getters = {
  getField
}

export const mutations = {
  updateField
}

export const actions = {
  async nuxtServerInit ({ dispatch, commit }, { req }) {
    const user = getUserFromCookie(req)

    if (user) {
      await dispatch('modules/user/setUSER', {
        email: user.email,
        uid: user.user_id,
        emailVerified: user.email_verified
      })
      await dispatch('modules/user/setUSERMETA', user.user_id)
    }
  },
  async nuxtClientInit ({ state, dispatch }, context, req) {
    const user = getUserFromBrowserCookie()

    if (user) {
      await dispatch('modules/user/setUSER', {
        email: user.email,
        uid: user.user_id,
        emailVerified: user.email_verified
      })
      await dispatch('modules/user/setUSERMETA', user.user_id)
    }
  }
}
