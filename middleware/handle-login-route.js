export default function ({ store, redirect }) {
  if (store.getters['modules/user/isAuthenticated']) {
    if (store.getters['modules/user/isAdmin']) {
      return redirect('/admin/dashboard')
    }

    return redirect('/profile/dashboard')
  }
}
