export default function ({ store, redirect }) {
  if (!store.getters['modules/user/isAuthenticated']) {
    return redirect('/auth/login')
  }

  if (!store.getters['modules/user/isAdmin']) {
    return redirect('/auth/login')
  }
}
