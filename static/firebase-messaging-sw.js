
importScripts('https://www.gstatic.com/firebasejs/7.9.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.9.1/firebase-messaging.js')

firebase.initializeApp({
  apiKey: 'AIzaSyB8PpYAnDsDWmy6v9rB0hRU6ZnxE610EYc',
  authDomain: 'social-books-creator.firebaseapp.com',
  databaseURL: 'https://social-books-creator.firebaseio.com',
  projectId: 'social-books-creator',
  storageBucket: 'social-books-creator.appspot.com',
  messagingSenderId: '68140448391',
  appId: '1:68140448391:web:e48313b58abe7501'
})

const messaging = firebase.messaging()

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: payload.notification.icon
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});