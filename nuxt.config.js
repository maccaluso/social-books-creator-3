import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'spa',

  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'msapplication-TileImage', content: '/ms-icon-144x144.png' },
      { name: 'theme-color', content: '#ffffff' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || 'Temporary description' }
    ],
    script: [
      { src: 'https://widget.cloudinary.com/v2.0/global/all.js' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  env: {
    // FIREBASE: {
    //   API_KEY: 'AIzaSyB8PpYAnDsDWmy6v9rB0hRU6ZnxE610EYc',
    //   PROJECT_ID: 'social-books-creator',
    //   SENDER_ID: '68140448391',
    //   DATABASE_NAME: 'social-books-creator'
    // },
    CLOUDINARY_CLOUD_NAME: process.env.CLOUDINARY_CLOUD_NAME || 'maccaluso'
  },

  vue: {
    config: {
      productionTip: true,
      devtools: true
    }
  },

  loading: { color: '#42AB3D' },

  css: [
  ],

  plugins: [
    '~/plugins/i18n.js',
    '~/plugins/cloudinary-upload.js'
  ],

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify'
  ],

  modules: [
    '@nuxtjs/toast',
    'nuxt-client-init-module',
    'nuxt-custom-headers',
    'nuxt-webfontloader'
  ],

  manifest: {
    gcm_sender_id: ' 103953800507 '
  },

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#42AB3D',
          secondary: '#8bc0dd',
          accent: '9ECCEE'
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  toast: {
    position: 'bottom-right',
    register: [
      {
        name: 'sbc_error',
        message: (payload) => {
          if (!payload.message) { return 'Oops... Something went wrong...' }
          return 'Oops... ' + payload.message
        },
        options: {
          type: 'error',
          iconPack: 'mdi',
          icon: 'mdi-alert-circle-outline',
          containerClass: 'sbc-toasted-container',
          className: 'sbc-toasted-content',
          action: {
            text: 'Dismiss',
            onClick: (e, toastObject) => {
              toastObject.goAway(0)
            }
          },
          duration: 10000
        }
      },
      {
        name: 'sbc_success',
        message: (payload) => {
          if (!payload.message) { return 'Great! Everything\'s good' }
          return payload.message
        },
        options: {
          type: 'success',
          iconPack: 'mdi',
          icon: 'mdi-checkbox-marked-circle-outline',
          containerClass: 'sbc-toasted-container',
          className: 'sbc-toasted-content',
          action: {
            text: 'Dismiss',
            onClick: (e, toastObject) => {
              toastObject.goAway(0)
            }
          },
          duration: 10000
        }
      },
      {
        name: 'sbc_warning',
        message: (payload) => {
          if (!payload.message) { return 'Warning! Something strange here' }
          return payload.message
        },
        options: {
          type: 'warning',
          iconPack: 'mdi',
          icon: 'mdi-alert-outline',
          containerClass: 'sbc-toasted-container',
          className: 'sbc-toasted-content',
          action: {
            text: 'Dismiss',
            onClick: (e, toastObject) => {
              toastObject.goAway(0)
            }
          },
          duration: 10000
        }
      }
    ]
  },

  webfontloader: {
    google: {
      families: [
        'Montserrat:400,700',
        'Open+Sans:400,700',
        'Merriweather:400,700',
        'PT+Serif:400,700',
        'Crimson+Text:400,700',
        'Courier+Prime:400,700',
        'Inconsolata:400,700',
        'Roboto+Mono:400,700'
      ]
    }
  },

  build: {
    babel: {
      presets ({ isServer }) {
        return [
          [
            require.resolve('@nuxt/babel-preset-app'),
            // require.resolve('@nuxt/babel-preset-app-edge'), // For nuxt-edge users
            {
              buildTarget: isServer ? 'server' : 'client',
              corejs: { version: 3 }
            }
          ]
        ]
      }
    },
    extend (config, { isDev, isClient }) {
      if (isDev) {
        config.resolve.alias.config = '~/config/development'
      } else {
        config.resolve.alias.config = '~/config/production'
      }

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
