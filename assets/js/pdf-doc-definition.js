import pdfMake from 'pdfmake'
import { getCoverTextAlignment } from './pdf-utils'

export const docDefinition = (book, content) => {
  return {
    info: {
      title: book.data.title,
      author: book.data.author,
      subject: book.data.abstract,
      keywords: 'keywords for document'
    },
    header: (currentPage, pageCount, pageSize) => {
      pdfMake.pageLayout = { ...pdfMake.pageLayout, currentPage, pageCount, pageSize }
      return ''
    },
    footer (currentPage, pageCount) {
      if (currentPage > 2 && currentPage < pageCount) {
        if (currentPage % 2 === 0) {
          return [{
            margin: [60, 10, 60, 10],
            layout: {
              hLineColor: i => (i === 0) ? book.data.format.typography.body.color : '',
              vLineWidth: i => 0,
              hLineWidth: i => (i === 0) ? 1 : 0
            },
            table: {
              widths: ['*', 160, 160],
              body: [
                [
                  { text: ' ' },
                  { text: ' ' },
                  { text: ' ' }
                ],
                [
                  { text: `${currentPage}`, alignment: 'left', fontSize: 10, color: book.data.format.typography.body.color },
                  { text: '', alignment: 'center', fontSize: 10 },
                  { text: '', alignment: 'right', fontSize: 10 }
                ]
              ]
            }
          }]
        } else {
          return [{
            margin: [60, 10, 60, 10],
            layout: {
              hLineColor: i => (i === 0) ? book.data.format.typography.body.color : '',
              vLineWidth: i => 0,
              hLineWidth: i => (i === 0) ? 1 : 0
            },
            table: {
              widths: ['*', 160, 160],
              body: [
                [
                  { text: ' ' },
                  { text: ' ' },
                  { text: ' ' }
                ],
                [
                  { text: '', alignment: 'left', fontSize: 10 },
                  { text: '', alignment: 'center', fontSize: 10 },
                  { text: `${currentPage}`, alignment: 'right', fontSize: 10, color: book.data.format.typography.body.color }
                ]
              ]
            }
          }]
        }
      }
    },
    content,
    pageSize: book.data.format.pageSize,
    pageOrientation: book.data.format.orientation,
    pageMargins: [60, 80, 60, 80],
    defaultStyle: {
      font: 'Roboto',
      fontSize: 14,
      lineHeight: 1.25
    },
    styles: {
      coverAuthorName: {
        font: book.data.format.typography.headings.family.replace('+', ''),
        fontSize: 18,
        bold: true,
        color: book.data.cover.authorColor,
        background: book.data.cover.authorBackgroundColor,
        alignment: getCoverTextAlignment(book.data.cover.authorPosition)
      },
      coverTitle: {
        font: book.data.format.typography.headings.family.replace('+', ''),
        fontSize: 24,
        bold: true,
        color: book.data.cover.titleColor,
        background: book.data.cover.titleBackgroundColor,
        alignment: getCoverTextAlignment(book.data.cover.titlePosition)
      },
      chapterTitle: {
        font: book.data.format.typography.headings.family.replace('+', ''),
        bold: true,
        color: book.data.format.typography.headings.color
      },
      subChapterTitle: {
        font: book.data.format.typography.headings.family.replace('+', ''),
        bold: true,
        color: book.data.format.typography.headings.color
      },
      headings: {
        font: book.data.format.typography.headings.family.replace('+', ''),
        bold: true,
        color: book.data.format.typography.headings.color
      },
      body: {
        font: book.data.format.typography.body.family.replace('+', ''),
        color: book.data.format.typography.body.color
      },
      list: {
        font: book.data.format.typography.body.family.replace('+', ''),
        color: book.data.format.typography.body.color
      },
      checkList: {
        font: book.data.format.typography.body.family.replace('+', ''),
        color: book.data.format.typography.body.color
      },
      chapterIndex: {
        font: book.data.format.typography.headings.family.replace('+', ''),
        color: book.data.format.typography.headings.color
      },
      code: {
        font: book.data.format.typography.pre.family.replace('+', ''),
        color: book.data.format.typography.pre.color
      },
      quote: {
        font: book.data.format.typography.body.family.replace('+', ''),
        color: book.data.format.typography.body.color
      },
      caption: {
        font: book.data.format.typography.body.family.replace('+', ''),
        color: book.data.format.typography.body.color
      }
    }
  }
}
