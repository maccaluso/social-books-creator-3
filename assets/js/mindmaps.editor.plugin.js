// import MindElixir, { E } from 'mind-elixir'
import { v4 as uuidv4 } from 'uuid'
// import html2canvas from 'html2canvas'

import MindElixir from 'mind-elixir'

class MindMaps {
  mind = null
  elID = null
  mapDefaults = null

  static get toolbox () {
    return {
      title: 'MindMaps',
      icon: `
        <svg width="17" height="15" viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M9.17925 4H11C11.5523 4 12 3.55228 12 3V1C12 0.447715 11.5523 0 11 0H6C5.44772 0 5 0.447715 5 1V3C5 3.55228 5.44772 4 6 4H7.375L3 11H1C0.447715 11 0 11.4477 0 12V14C0 14.5523 0.447715 15 1 15H6C6.55228 15 7 14.5523 7 14V12C7 11.4477 6.55228 11 6 11H4.17925L8.27712 4.4434L12.375 11H11C10.4477 11 10 11.4477 10 12V14C10 14.5523 10.4477 15 11 15H16C16.5523 15 17 14.5523 17 14V12C17 11.4477 16.5523 11 16 11H13.5542L9.17925 4ZM11 1H6V3H11V1ZM11 12V14H16V12H11ZM1 12H6V14H1L1 12Z"/>
        </svg>
      `
    }
  }

  constructor ({ data }) {
    this.data = data
    this.elID = 'map' + uuidv4()
    this.mapDefaults = {
      el: '#' + this.elID,
      direction: MindElixir.LEFT,
      draggable: true,
      editable: false,
      contextMenu: true,
      toolBar: false,
      nodeMenu: false,
      keypress: false
    }
  }

  render () {
    const div = document.createElement('div')
    div.id = this.elID
    div.style.height = '500px'
    div.style.width = '100%'
    div.style.position = 'relative'

    return div
  }

  rendered () {
    if (this.data.mapData) {
      this.mapDefaults.data = this.data.mapData
    } else {
      this.mapDefaults.editable = true
      this.mapDefaults.toolBar = true
      // this.mapDefaults.editable = true
      this.mapDefaults.data = MindElixir.new('new topic')
    }

    this.mind = new MindElixir(this.mapDefaults)

    // this.mind.bus.addListener('operation', (operation) => {
    //   console.log(operation.name)
    //   // return {
    //   //   name: action name,
    //   //   obj: target object
    //   // }

    //   // name: [insertSibling|addChild|removeNode|beginEdit|finishEdit]
    //   // obj: target

    //   // name: moveNode
    //   // obj: {from:target1,to:target2}
    // })
    // this.mind.bus.addListener('selectNode', (node) => {
    //   console.log(node)
    // })

    this.mind.init()
  }

  save () {
    return {
      mapData: this.mind.getAllData()
    }
  }
}

export default MindMaps
