class ClodinaryImageUpload {
  cloudinaryURL = null
  cloudinaryObject = null
  imageEL = null

  static get toolbox () {
    return {
      title: 'ClodinaryImageUpload',
      icon: `
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M18 16V2C18 0.9 17.1 0 16 0H2C0.9 0 0 0.9 0 2V16C0 17.1 0.9 18 2 18H16C17.1 18 18 17.1 18 16ZM5.5 10.5L8 13.51L11.5 9L16 15H2L5.5 10.5Z"/>
        </svg>      
      `
      // icon: `
      //   <svg width="17" height="15" viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      //     <path d="M9.17925 4H11C11.5523 4 12 3.55228 12 3V1C12 0.447715 11.5523 0 11 0H6C5.44772 0 5 0.447715 5 1V3C5 3.55228 5.44772 4 6 4H7.375L3 11H1C0.447715 11 0 11.4477 0 12V14C0 14.5523 0.447715 15 1 15H6C6.55228 15 7 14.5523 7 14V12C7 11.4477 6.55228 11 6 11H4.17925L8.27712 4.4434L12.375 11H11C10.4477 11 10 11.4477 10 12V14C10 14.5523 10.4477 15 11 15H16C16.5523 15 17 14.5523 17 14V12C17 11.4477 16.5523 11 16 11H13.5542L9.17925 4ZM11 1H6V3H11V1ZM11 12V14H16V12H11ZM1 12H6V14H1L1 12Z"/>
      //   </svg>
      // `
    }
  }

  constructor ({ data }) {
    this.data = data

    this.imageEL = document.createElement('img')
    this.imageEL.style.minHeight = '200px'
    this.imageEL.style.maxHeight = '100%'
    this.imageEL.style.maxWidth = '100%'
    this.imageEL.style.marginBottom = '15px'
  }

  render () {
    const div = document.createElement('div')
    div.style.position = 'relative'
    div.style.maxHeight = '500px'
    div.style.width = '100%'
    div.style.display = 'flex'
    div.style.flexDirection = 'column'
    div.style.alignItems = 'center'
    div.style.justifyContent = 'center'

    div.appendChild(this.imageEL)

    const input = document.createElement('input')
    input.setAttribute('type', 'file')
    input.setAttribute('accept', 'image/png, image/jpeg')
    input.addEventListener('change', () => {
      const formData = new FormData()
      formData.append('upload_preset', 'social-books-creator')
      formData.append('file', input.files[0])

      fetch('https://api.cloudinary.com/v1_1/gianluca-macaluso/upload', {
        method: 'POST',
        body: formData
      }).then(response => response.json()).then((data) => {
        this.cloudinaryObject = data
        this.cloudinaryURL = data.secure_url
        this.imageEL.setAttribute('src', data.secure_url)
      })
    })

    div.appendChild(input)

    return div
  }

  rendered () {
    if (this.data && this.data.cloudinaryURL) {
      this.imageEL.setAttribute('src', this.data.cloudinaryURL)
    }
  }

  save () {
    return {
      cloudinaryObject: this.cloudinaryObject,
      cloudinaryURL: this.cloudinaryURL
    }
  }
}

export default ClodinaryImageUpload
