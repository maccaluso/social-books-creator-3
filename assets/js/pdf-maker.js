import pdfMake from 'pdfmake'

import { docDefinition } from './pdf-doc-definition'
import {
  getCoverTextPosition,
  getTextSize,
  parseHeaderBlock,
  parseParagraphBlock,
  parseSimpleImageBlock,
  parseDelimiterBlock,
  parseListBlock,
  parseCheckListBlock,
  parseRawBlock,
  parseCodeBlock,
  parseQuoteBlock,
  parseEmbedBlock
  // parseMindMapsBlock
} from './pdf-utils'

pdfMake.fonts = {
  Montserrat: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783550/social-books-creator/fonts/Montserrat-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783548/social-books-creator/fonts/Montserrat-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783548/social-books-creator/fonts/Montserrat-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783548/social-books-creator/fonts/Montserrat-BoldItalic.ttf'
  },
  OpenSans: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783919/social-books-creator/fonts/OpenSans-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783918/social-books-creator/fonts/OpenSans-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783919/social-books-creator/fonts/OpenSans-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783919/social-books-creator/fonts/OpenSans-BoldItalic.ttf'
  },
  Roboto: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598788223/social-books-creator/fonts/Roboto-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598788237/social-books-creator/fonts/Roboto-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598788223/social-books-creator/fonts/Roboto-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598788232/social-books-creator/fonts/Roboto-BoldItalic.ttf'
  },
  Merriweather: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783521/social-books-creator/fonts/Merriweather-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783520/social-books-creator/fonts/Merriweather-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783521/social-books-creator/fonts/Merriweather-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783520/social-books-creator/fonts/Merriweather-BoldItalic.ttf'
  },
  PTSerif: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598782307/social-books-creator/fonts/PTSerif-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598782317/social-books-creator/fonts/PTSerif-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598782307/social-books-creator/fonts/PTSerif-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598782303/social-books-creator/fonts/PTSerif-BoldItalic.ttf'
  },
  CrimsonText: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783444/social-books-creator/fonts/CrimsonText-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783444/social-books-creator/fonts/CrimsonText-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783444/social-books-creator/fonts/CrimsonText-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783444/social-books-creator/fonts/CrimsonText-BoldItalic.ttf'
  },
  CourierPrime: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783426/social-books-creator/fonts/CourierPrime-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783425/social-books-creator/fonts/CourierPrime-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783426/social-books-creator/fonts/CourierPrime-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783426/social-books-creator/fonts/CourierPrime-BoldItalic.ttf'
  },
  Inconsolata: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783496/social-books-creator/fonts/Inconsolata-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783496/social-books-creator/fonts/Inconsolata-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783496/social-books-creator/fonts/Inconsolata-Regular.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598783496/social-books-creator/fonts/Inconsolata-Bold.ttf'
  },
  RobotoMono: {
    normal: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598784256/social-books-creator/fonts/RobotoMono-Regular.ttf',
    bold: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598784250/social-books-creator/fonts/RobotoMono-Bold.ttf',
    italics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598784250/social-books-creator/fonts/RobotoMono-Italic.ttf',
    bolditalics: 'https://res.cloudinary.com/gianluca-macaluso/raw/upload/v1598784251/social-books-creator/fonts/RobotoMono-BoldItalic.ttf'
  }
}

let pdf, def, _book, _store, _downloadType
let pdfContent, imagesPromises, arrayBuffers, imageStrings, chapterIndex

const initArrays = () => {
  pdfContent = []
  imagesPromises = []
  arrayBuffers = []
  imageStrings = []
  chapterIndex = []
}

const addPageBreak = (numberOfBreaksToInsert, where) => {
  for (let i = 0; i < numberOfBreaksToInsert; i++) {
    pdfContent.push({ text: '', pageBreak: where })
  }
}

const writePDFCover = () => {
  if (_book.data.cover.cloudinaryPath) {
    imagesPromises.push(fetch('https://cors-anywhere.herokuapp.com/' + _book.data.cover.cloudinaryPath))
  } else {
    imagesPromises.push(
      fetch(`
        https://cors-anywhere.herokuapp.com/
        https://res.cloudinary.com/gianluca-macaluso/image/upload/v1598527981/social-books-creator/cover-placeholder-A4-
        ${_book.data.format.orientation}.jpg
      `)
    )
  }

  pdfContent.push({ image: 'coverPlaceHolder' })

  if (_book.data.cover.showTitle) {
    pdfContent.push({
      text: ' ' + _store.state.modules.user.userMeta.firstName + ' ' + _store.state.modules.user.userMeta.surname + ' ',
      style: 'coverAuthorName',
      absolutePosition: getCoverTextPosition(_book.data.cover.authorPosition, pdfMake.pageLayout)
    })

    pdfContent.push({
      text: ' ' + _book.data.title + ' ',
      style: 'coverTitle',
      absolutePosition: getCoverTextPosition(_book.data.cover.titlePosition, pdfMake.pageLayout)
    })
  }

  addPageBreak(2, 'after')
}

const writePDFChapters = () => {
  _book.data.chapters.forEach((chapter, i) => {
    pdfContent.push({
      text: (i + 1) + ' - ' + chapter.title,
      pageBreak: 'after',
      style: 'chapterTitle',
      fontSize: getTextSize('chapterTitle', _book.data.format.typography.headings.size)
    })

    chapterIndex.push((i + 1) + ' - ' + chapter.title)

    addPageBreak(1, 'after')

    if (chapter.content.blocks) {
      chapter.content.blocks.forEach((chapterBlock, j) => {
        loopBlockTypes(chapterBlock)

        if (chapter.content.blocks.length - 1 === j) { addPageBreak(2, 'after') }
      })
    }

    chapter.children.forEach((subChapter, z) => {
      pdfContent.push({
        text: (i + 1) + '.' + (z + 1) + ') ' + subChapter.title,
        margin: [0, 20],
        style: 'subChapterTitle',
        fontSize: getTextSize('subChapterTitle', _book.data.format.typography.body.size)
      })
      chapterIndex.push((i + 1) + '.' + (z + 1) + ') ' + subChapter.title)

      if (subChapter.content.blocks) {
        subChapter.content.blocks.forEach((subChapterBlock, y) => {
          loopBlockTypes(subChapterBlock)

          if (subChapter.content.blocks.length - 1 === y) { addPageBreak(2, 'after') }
        })
      }
    })
  })
}

const writeChapterIndex = () => {
  pdfContent.push({
    text: 'Index',
    style: 'chapterTitle',
    fontSize: getTextSize('chapterTitle', _book.data.format.typography.headings.size)
  })
  pdfContent.push({
    ul: chapterIndex,
    margin: [0, 20],
    style: 'chapterIndex',
    fontSize: getTextSize('headings', 'medium'),
    pageBreak: 'after'
  })
}

const loopBlockTypes = (block) => {
  switch (block.type) {
    case 'header':
      pdfContent.push(parseHeaderBlock(block, _book.data.format.typography.headings.size))
      break
    case 'paragraph':
      pdfContent.push(parseParagraphBlock(block, _book.data.format.typography.body.size))
      break
    case 'simpleImage':
      imagesPromises.push(fetch('https://cors-anywhere.herokuapp.com/' + block.data.url))
      pdfContent.push(parseSimpleImageBlock(block).imagePlaceholder)

      if (block.data.caption) {
        pdfContent.push(parseSimpleImageBlock(block).caption)
      }
      break
    case 'delimiter':
      pdfContent.push(parseDelimiterBlock())
      break
    case 'list':
      pdfContent.push(parseListBlock(block, _book.data.format.typography.body.size))
      break
    case 'checklist':
      pdfContent.push(parseCheckListBlock(block, _book.data.format.typography.body.size))
      break
    case 'raw':
      pdfContent.push(parseRawBlock(block, _book.data.format.typography.pre.size, _book.data.format.typography.pre.color))
      break
    case 'code':
      pdfContent.push(parseCodeBlock(block, _book.data.format.typography.pre.size, _book.data.format.typography.pre.color))
      break
    case 'quote':
      pdfContent.push(parseQuoteBlock(block, _book.data.format.typography.body.size, _book.data.format.typography.body.color))
      break
    case 'embed':
      imagesPromises.push(fetch('https://cors-anywhere.herokuapp.com/' + parseEmbedBlock(block).posterUrl))
      pdfContent.push({ image: 'placeHolder', margin: [0, 5] })

      pdfContent.push(parseEmbedBlock(block).content)
      break
    case 'clodinaryImageUpload':
      imagesPromises.push(fetch('https://cors-anywhere.herokuapp.com/' + block.data.cloudinaryURL))
      pdfContent.push(parseSimpleImageBlock(block).imagePlaceholder)
      break
    // case 'mindMaps':
    //   parseMindMapsBlock(block)
    //   break
  }
}

const downloadImages = () => {
  Promise.all(imagesPromises)
    .then((response) => {
      response.forEach((imageResponse) => {
        arrayBuffers.push(imageResponse.arrayBuffer())
      })

      processArrayBuffers()
    })
}

const processArrayBuffers = () => {
  Promise.all(arrayBuffers).then((response) => {
    response.forEach((buffer) => {
      convertImageToBase64(buffer)
    })

    def = docDefinition(_book, pdfContent)
    updatePdfContentImages()
    generatePDF()
  })
}

const convertImageToBase64 = (buffer) => {
  const base64Flag = 'data:image/jpg;base64,'
  let binary = ''
  const bytes = [].slice.call(new Uint8Array(buffer))

  bytes.forEach((b) => {
    binary = binary + String.fromCharCode(b)
  })

  const imageStr = window.btoa(binary)

  imageStrings.push(base64Flag + imageStr)
}

const updatePdfContentImages = () => {
  let counter = 0

  pdfContent.forEach((pdfBlock, j) => {
    if (pdfBlock.image) {
      pdfBlock.image = imageStrings[counter]

      pdfBlock.width = 475.28

      // if (j === 0) {
      //   pdfBlock.width = pdfMake.pageLayout ? pdfMake.pageLayout.width - def.pageMargins[0] - def.pageMargins[2] : 0
      //   pdfBlock.height = pdfMake.pageLayout ? pdfMake.pageLayout.height - def.pageMargins[1] - def.pageMargins[3] : 0
      // } else {
      //   pdfBlock.width = pdfMake.pageLayout ? pdfMake.pageLayout.width - def.pageMargins[0] - def.pageMargins[2] : 475.28
      // }

      counter++
    }
  })
}

const generatePDF = () => {
  pdf = pdfMake.createPdf(def)

  pdf.getBase64((data) => {
    const fileContents = 'data:application/pdf;base64,' + data
    const formData = new FormData()
    formData.append('upload_preset', 'social-books-creator')
    formData.append('file', fileContents)

    fetch('https://api.cloudinary.com/v1_1/gianluca-macaluso/upload', {
      method: 'POST',
      body: formData
    }).then(response => response.json()).then((data) => {
      const segments = data.secure_url.split('/')
      _store.dispatch('modules/books/updateBookPDF', {
        book: _book,
        url: data.secure_url,
        name: segments[segments.length - 1],
        pages: data.pages
      }).then(() => {
        switch (_downloadType) {
          case 'open':
            pdf.open(_book.data.title.replace)
            break
          case 'download':
            pdf.download(_book.data.title.replace(' ', '-').toLowerCase())
            break
          case 'print':
            pdf.print(_book.data.title.replace)
            break
        }

        initArrays()

        _store.commit('modules/pdf/SET_PDF_IS_BUILDING', null)
        console.log('end process')
      })
    })
  })
}

export const download = (book, store, downloadType) => {
  _book = book
  _store = store
  _downloadType = downloadType

  _store.commit('modules/pdf/SET_PDF_IS_BUILDING', book.id)

  initArrays()
  writePDFCover()
  writePDFChapters()

  if (_book.data.format.chapterIndex) {
    writeChapterIndex()
  }

  downloadImages()
}
