/* eslint-disable no-unused-vars */
import go from 'gojs'
const $ = go.GraphObject.make

class MindMaps {
  static get toolbox () {
    return {
      title: 'MindMaps',
      icon: `
        <svg width="17" height="15" viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M9.17925 4H11C11.5523 4 12 3.55228 12 3V1C12 0.447715 11.5523 0 11 0H6C5.44772 0 5 0.447715 5 1V3C5 3.55228 5.44772 4 6 4H7.375L3 11H1C0.447715 11 0 11.4477 0 12V14C0 14.5523 0.447715 15 1 15H6C6.55228 15 7 14.5523 7 14V12C7 11.4477 6.55228 11 6 11H4.17925L8.27712 4.4434L12.375 11H11C10.4477 11 10 11.4477 10 12V14C10 14.5523 10.4477 15 11 15H16C16.5523 15 17 14.5523 17 14V12C17 11.4477 16.5523 11 16 11H13.5542L9.17925 4ZM11 1H6V3H11V1ZM11 12V14H16V12H11ZM1 12H6V14H1L1 12Z"/>
        </svg>
      `
    }
  }

  constructor ({ data }) {
    this.data = data

    this.goConfig = {
      locationSpot: go.Spot.Center,
      locationObjectName: 'SHAPE',
      selectionAdorned: false,
      resizable: true,
      resizeObjectName: 'SHAPE',
      rotatable: true,
      rotateObjectName: 'SHAPE',
      layoutConditions: go.Part.LayoutStandard & ~go.Part.LayoutNodeSized
    }

    this.goShapeConfig = {
      name: 'SHAPE',
      width: 70,
      height: 70,
      stroke: '#C2185B',
      fill: '#F48FB1',
      strokeWidth: 3
    }

    this.diagram = $(go.Diagram, this.container, {
      'undoManager.isEnabled': true,
      'grid.visible': true,
      'grid.gridCellSize': new go.Size(30, 30)
    })

    // this.diagram.nodeTemplate = $(
    //   go.Node,
    //   'Vertical',
    //   this.goConfig,
    //   new go.Binding('layerName', 'isHighlighted', function (h) { return h ? 'Foreground' : '' }).ofObject(),
    //   $(go.Shape, this.goShapeConfi)
    // )

    // this.diagram.startTransaction('new object')
    // this.diagram.model.addNodeData({})
    // this.diagram.model.addNodeData({})
    // this.diagram.commitTransaction('new object')
  }

  render () {
    this.container = document.createElement('div')
    this.container.style.height = '500px'
    this.container.style.width = '100%'
    this.container.style.position = 'relative'

    return this.container
  }

  rendered () {
    console.log('mindmap rendered', this.container)
  }

  save () {
    return {}
  }
}

export default MindMaps
