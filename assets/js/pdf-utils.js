// import { v4 as uuidv4 } from 'uuid'
// import MindElixir from 'mind-elixir'

const pageMargins = { left: 60, top: 80, right: 60, bottom: 80 }
const modifiers = {
  addLeftPadding: 7,
  subtractBottomPadding: 130,
  titleCharacterSize: 24
}

export const getCoverTextPosition = (positionString, pageLayout) => {
  switch (positionString) {
    case 'leftTop':
      return {
        x: pageMargins.left + modifiers.addLeftPadding,
        y: pageMargins.top
      }
    case 'leftCenter':
      return {
        x: pageMargins.left + modifiers.addLeftPadding,
        y: pageLayout ? pageLayout.height / 2 : modifiers.titleCharacterSize
      }
    case 'leftBottom':
      return {
        x: pageMargins.left + modifiers.addLeftPadding,
        y: pageLayout ? pageLayout.height - modifiers.subtractBottomPadding : modifiers.titleCharacterSize
      }
    case 'centerTop':
      return {
        x: 0,
        y: pageMargins.top
      }
    case 'centerCenter':
      return {
        x: 0,
        y: pageLayout ? pageLayout.height / 2 : modifiers.titleCharacterSize
      }
    case 'centerBottom':
      return {
        x: 0,
        y: pageLayout ? pageLayout.height - modifiers.subtractBottomPadding : modifiers.titleCharacterSize
      }
    case 'rightTop':
      return {
        x: 0,
        y: pageMargins.top
      }
    case 'rightCenter':
      return {
        x: 0,
        y: pageLayout ? pageLayout.height / 2 : modifiers.titleCharacterSize
      }
    case 'rightBottom':
      return {
        x: 0,
        y: pageLayout ? pageLayout.height - modifiers.subtractBottomPadding : modifiers.titleCharacterSize
      }
    default:
      return {}
  }
}

export const getCoverTextAlignment = (positionString) => {
  if (positionString === 'leftTop' || positionString === 'leftCenter' || positionString === 'leftBottom') {
    return 'left'
  }

  if (positionString === 'centerTop' || positionString === 'centerCenter' || positionString === 'centerBottom') {
    return 'center'
  }

  if (positionString === 'rightTop' || positionString === 'rightCenter' || positionString === 'rightBottom') {
    return 'right'
  }

  return 'left'
}

export const decodeEntities = (str) => {
  const shifter = document.createElement('div')
  shifter.innerHTML = str

  const links = shifter.getElementsByTagName('a')

  if (links.length > 0) {
    links.forEach((link) => {
      const newLink = document.createElement('a')
      newLink.setAttribute('href', link.getAttribute('href'))
      newLink.textContent = link.textContent

      const bold = document.createElement('b')
      const italic = document.createElement('i')

      const nodes = []

      if (link.childNodes.length > 0) {
        link.childNodes.forEach((childNode) => {
          nodes.push(childNode)

          if (childNode.childNodes.length > 0) {
            childNode.childNodes.forEach((subChildNode) => {
              if (subChildNode.tagName) {
                nodes.push(subChildNode)
              }
            })
          }
        })
      }

      switch (nodes.length) {
        case 1:
          if (nodes[0].tagName === 'B') {
            bold.appendChild(newLink)
            shifter.replaceChild(bold, link)
          }
          if (nodes[0].tagName === 'I') {
            italic.appendChild(newLink)
            shifter.replaceChild(italic, link)
          }
          break
        case 2:
          italic.appendChild(newLink)
          bold.appendChild(italic)

          shifter.replaceChild(bold, link)
          break
      }
    })
  }
  // console.log(shifter.innerHTML)

  const textArea = document.createElement('textarea')
  textArea.innerHTML = shifter.innerHTML
  return textArea.value
}

export const parseParagraph = (str) => {
  const chunks = parseBolds(str)

  chunks.forEach((chunk, i) => {
    chunks[i] = parseItalics(chunk)
  })

  chunks.forEach((chunk, i) => {
    chunks[i] = parseLinks(chunk)
  })

  return chunks
}

export const parseBolds = (str) => {
  const chunks = []
  let decoded = decodeEntities(str)
  const bolds = decoded.match(/<b>(.*?)<\/b>/g)

  if (bolds) {
    bolds.forEach((bold, index) => {
      chunks.push(decoded.split(bold)[0])
      chunks.push({
        text: bold.replace('<b>', '').replace('</b>', ''),
        bold: true
      })

      if (index === bolds.length - 1) {
        chunks.push(decoded.split(bold)[1])
      }

      const start = decoded.indexOf(bold)
      const end = start + bold.length

      decoded = decoded.split(decoded.substring(0, end))[1]
    })
  } else {
    chunks.push(decoded)
  }

  return chunks
}

export const parseItalics = (chunk) => {
  let string = chunk
  if (chunk.text) { string = chunk.text }

  const italics = string.match(/<i>(.*?)<\/i>/g)
  if (italics) {
    const newChunk = { text: [] }

    italics.forEach((italic, index) => {
      newChunk.text.push(string.split(italic)[0])
      newChunk.text.push({
        text: italic.replace('<i>', '').replace('</i>', ''),
        italics: true,
        bold: chunk.bold === true
      })

      if (index === italics.length - 1) {
        newChunk.text.push(string.split(italic)[1])
      }

      const start = string.indexOf(italic)
      const end = start + italic.length

      string = string.split(string.substring(0, end))[1]
    })

    return newChunk
  } else {
    return chunk
  }
}

export const parseLinks = (chunk) => {
  let string = chunk
  if (chunk.text) { string = chunk.text }

  if (typeof chunk.text === 'object') {
    chunk.text.forEach((subChunk) => {
      string = subChunk
      if (subChunk.text) { string = subChunk.text }

      const link = string.match(/<a(.*?)>(.*?)<\/a>/g)

      if (link) {
        subChunk.text = []
        subChunk.text.push(string.split(link)[0])
        subChunk.text.push({
          text: link[0].replace(/<a(.*?)>/g, '').replace('</a>', ''),
          link: link[0].match(/href="([^'"]+)/g)[0].replace('href="', ''),
          bold: subChunk.bold === true,
          italics: subChunk.italics === true,
          decoration: 'underline'
        })
        subChunk.text.push(string.split(link)[1])
      }
    })
  }

  const links = string.match(/<a(.*?)>(.*?)<\/a>/g)

  if (links) {
    const newChunk = { text: [] }

    links.forEach((link, index) => {
      newChunk.text.push(string.split(link)[0])
      newChunk.text.push({
        text: link.replace(/<a(.*?)>/g, '').replace('</a>', ''),
        link: link.match(/href="([^'"]+)/g)[0].replace('href="', ''),
        bold: chunk.bold === true,
        italics: chunk.italics === true,
        decoration: 'underline'
      })

      if (index === links.length - 1) {
        newChunk.text.push(string.split(link)[1])
      }

      const start = string.indexOf(link)
      const end = start + link.length

      string = string.split(string.substring(0, end))[1]
    })

    return newChunk
  } else {
    return chunk
  }
}

export const parseQuotes = (str) => {
  const decoded = decodeEntities(str)
  return decoded.replace('<br>', '')
}

export const getTextSize = (type, sizeString) => {
  if (type === 'chapterTitle' || type === 'headings') {
    switch (sizeString) {
      case 'small':
        return 14
      case 'medium':
        return 18
      case 'large':
        return 22
      default:
        return 18
    }
  }
  if (type === 'body' || type === 'subChapterTitle') {
    switch (sizeString) {
      case 'small':
        return 12
      case 'medium':
        return 14
      case 'large':
        return 16
      default:
        return 14
    }
  }
  if (type === 'raw' || type === 'code') {
    switch (sizeString) {
      case 'small':
        return 10
      case 'medium':
        return 12
      case 'large':
        return 14
      default:
        return 12
    }
  }
}

export const parseHeaderBlock = (block, sizeString) => {
  return {
    text: block.data.text,
    margin: [0, 20],
    style: 'headings',
    fontSize: getTextSize('headings', sizeString)
  }
}

export const parseParagraphBlock = (block, sizeString) => {
  return {
    text: parseParagraph(block.data.text),
    margin: [0, 10],
    style: 'body',
    fontSize: getTextSize('body', sizeString)
  }
}

export const parseSimpleImageBlock = (block) => {
  return {
    imagePlaceholder: { image: 'placeHolder', margin: [0, block.data.caption ? 5 : 20] },
    caption: {
      text: decodeEntities(block.data.caption),
      margin: [0, 5, 0, 20],
      fontSize: 10,
      style: 'caption'
    }
  }
}

export const parseDelimiterBlock = () => {
  return { text: '***', margin: [0, 20], alignment: 'center', style: 'body' }
}

export const parseListBlock = (block, sizeString) => {
  const items = []
  block.data.items.forEach((item) => {
    items.push(item)
  })

  return {
    ol: items,
    margin: [0, 20],
    style: 'list',
    fontSize: getTextSize('body', sizeString)
  }
}

export const parseCheckListBlock = (block, sizeString) => {
  return {
    ul: block.data.items,
    margin: [0, 20],
    style: 'checkList',
    fontSize: getTextSize('body', sizeString)
  }
}

export const parseCodeBlock = (block, sizeString, color) => {
  return {
    table: {
      widths: ['*'],
      body: [
        [
          {
            text: block.data.code,
            color: '#48525B',
            borderColor: ['#ccc', '#ccc', '#ccc', '#ccc']
          }
        ]
      ]
    },
    layout: {
      paddingTop: () => { return 10 },
      paddingRight: () => { return 15 },
      paddingBottom: () => { return 10 },
      paddingLeft: () => { return 15 },
      fillColor: () => { return '#fff' }
    },
    margin: [0, 20],
    style: 'code',
    fontSize: getTextSize('raw', sizeString)
  }
}

export const parseRawBlock = (block, sizeString) => {
  return {
    table: {
      widths: ['*'],
      body: [
        [
          {
            text: decodeEntities(block.data.html),
            color: '#ffffff',
            borderColor: ['#48525B', '#48525B', '#48525B', '#48525B']
          }
        ]
      ]
    },
    layout: {
      paddingTop: () => { return 10 },
      paddingRight: () => { return 15 },
      paddingBottom: () => { return 10 },
      paddingLeft: () => { return 15 },
      fillColor: () => { return '#48525B' }
    },
    margin: [0, 20],
    style: 'code',
    fontSize: getTextSize('code', sizeString)
  }
}

export const parseQuoteBlock = (block, sizeString, color) => {
  return {
    table: {
      widths: [100, '*'],
      body: [
        [
          {
            text: '',
            border: [false, false, false, false]
          },
          {
            border: [false, false, true, false],
            borderColor: ['#000', '#000', color, '#000'],
            text: '"' + parseQuotes(block.data.text) + '"',
            italics: true,
            alignment: 'right'
          }
        ],
        [
          {
            text: '',
            border: [false, false, false, false]
          },
          {
            border: [false, false, true, false],
            borderColor: ['#000', '#000', color, '#000'],
            text: decodeEntities(block.data.caption),
            bold: true,
            fontSize: 10,
            alignment: 'right'
          }
        ]
      ]
    },
    layout: {
      paddingRight: () => { return 20 }
    },
    margin: [0, 20],
    style: 'quote',
    fontSize: getTextSize('body', sizeString)
  }
}

export const parseEmbedBlock = (block) => {
  const ytURL = block.data.embed
  const ytID = ytURL.split('/embed/')[1]
  const posterURL = 'https://img.youtube.com/vi/' + ytID + '/hqdefault.jpg'

  return {
    posterUrl: posterURL,
    content: {
      text: decodeEntities(block.data.caption),
      link: 'https://www.youtube.com/watch?v=' + ytID,
      margin: [0, 5, 0, 20],
      fontSize: 10,
      style: 'caption'
    }
  }
}

// export const parseMindMapsBlock = (block) => {
//   // console.log(block)
//   createTemporaryMindMap(block.data.mapData)
// }

// export const createTemporaryMindMap = (data) => {
//   const div = document.createElement('div')
//   div.id = 'map'
//   div.style.height = '500px'
//   div.style.width = '500px'
//   div.style.position = 'absolute'
//   div.style.top = 0
//   div.style.left = 0
//   div.style.zIndex = 1000000000
//   div.style.backgroundColor = 'red'

//   document.body.appendChild(div)

//   // console.log(MindElixir)

//   // const mind = MindElixir({
//   //   data,
//   //   el: '#map',
//   //   direction: MindElixir.LEFT,
//   //   draggable: false,
//   //   editable: false,
//   //   contextMenu: false,
//   //   toolBar: false,
//   //   nodeMenu: false,
//   //   keypress: false
//   // })
//   // mind.init()

//   // console.log(mind)
//   // console.log(div)
// }
