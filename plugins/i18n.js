import Vue from 'vue'

import vuexI18n from 'vuex-i18n/dist/vuex-i18n.umd.js'

import enUS from '~/locales/en-US.js'
import esES from '~/locales/es-ES.js'
import deDE from '~/locales/de-DE.js'
import itIT from '~/locales/it-IT.js'
import nlBE from '~/locales/nl-BE.js'
import roRO from '~/locales/ro-RO.js'

export default function ({ store }) {
  Vue.use(vuexI18n.plugin, store)

  Vue.i18n.add('English', enUS)
  Vue.i18n.add('Deutsch', deDE)
  Vue.i18n.add('Español', esES)
  Vue.i18n.add('Italiano', itIT)
  Vue.i18n.add('Nederlands', nlBE)
  Vue.i18n.add('Română', roRO)

  Vue.i18n.fallback('English')

  // Vue.i18n.set('Italiano')
  Vue.i18n.set('English')
  store.commit('modules/app/SET_CURRENT_LOCALE_INDEX', 0)
}
