import pdfMake from 'pdfmake'
import pdfFonts from 'pdfmake/build/vfs_fonts'
import htmlToPdfMake from 'html-to-pdfmake'
pdfMake.vfs = pdfFonts.pdfMake.vfs

const emptyRetro = { text: '', pageBreak: 'after' }

// eslint-disable-next-line no-unused-vars
const utils = {
  pdfDownloader2 (book) {
    const pdfContent = [
      { text: book.data.title, pageBreak: 'after' },
      emptyRetro
    ]
    const imagesPromises = []
    const arrayBuffers = []

    book.data.chapters.forEach((chapter, i) => {
      pdfContent.push({
        text: (i + 1) + ' - ' + chapter.title,
        pageBreak: 'after'
      })
      pdfContent.push(emptyRetro)

      chapter.content.blocks.forEach((chapterBlock, j) => {
        switch (chapterBlock.type) {
          case 'header':
            pdfContent.push({ text: chapterBlock.data.text, margin: [0, 20] })
            break
          case 'paragraph':
            pdfContent.push({ text: chapterBlock.data.text, margin: [0, 10] })
            break
          case 'simpleImage': {
            // console.log(chapterBlock.data)
            imagesPromises.push(fetch('https://cors-anywhere.herokuapp.com/' + chapterBlock.data.url))
            pdfContent.push({ image: 'placeHolder', margin: [0, 20] })
            break
          }
        }

        if (chapter.content.blocks.length - 1 === j) { pdfContent.push(emptyRetro) }
      })

      chapter.children.forEach((subChapter, z) => {
        pdfContent.push({ text: (i + 1) + '.' + (z + 1) + ') ' + subChapter.title, margin: [0, 20] })

        subChapter.content.blocks.forEach((subChapterBlock, y) => {
          switch (subChapterBlock.type) {
            case 'header':
              pdfContent.push({ text: subChapterBlock.data.text, margin: [0, 20] })
              break
            case 'paragraph':
              pdfContent.push({ text: subChapterBlock.data.text, margin: [0, 10] })
              break
            case 'simpleImage': {
              // console.log(subChapterBlock.data)
              imagesPromises.push(fetch('https://cors-anywhere.herokuapp.com/' + subChapterBlock.data.url))
              pdfContent.push({ image: 'placeHolder', margin: [0, 20] })
              break
            }
          }

          if (subChapter.content.blocks.length - 1 === y) { pdfContent.push(emptyRetro) }
        })
      })
    })

    Promise.all(imagesPromises).then((response) => {
      response.forEach((imageResponse) => {
        arrayBuffers.push(imageResponse.arrayBuffer())
      })

      const imageStrings = []

      Promise.all(arrayBuffers).then((response) => {
        response.forEach((buffer) => {
          const base64Flag = 'data:image/jpg;base64,'
          let binary = ''
          const bytes = [].slice.call(new Uint8Array(buffer))

          bytes.forEach((b) => {
            binary = binary + String.fromCharCode(b)
          })

          const imageStr = window.btoa(binary)

          imageStrings.push(base64Flag + imageStr)
        })

        let counter = 0

        pdfContent.forEach((pdfBlock, j) => {
          if (pdfBlock.image) {
            pdfBlock.image = imageStrings[counter]
            pdfBlock.width = 475.276
            counter++
          }
        })

        const docDefinition = {
          info: {
            title: book.data.title,
            author: 'john doe',
            subject: book.data.abstract,
            keywords: 'keywords for document'
          },
          content: pdfContent,
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [60, 80, 60, 120],
          footer (currentPage, pageCount) {
            if (currentPage > 3) {
              if (currentPage % 2 === 0) {
                return [{
                  margin: [60, 30, 60, 30],
                  layout: {
                    hLineColor: i => (i === 0) ? 'lightgray' : '',
                    vLineWidth: i => 0,
                    hLineWidth: i => (i === 0) ? 1 : 0
                  },
                  table: {
                    widths: ['*', 160, 160],
                    body: [
                      [
                        { text: ' ' },
                        { text: ' ' },
                        { text: ' ' }
                      ],
                      [
                        // { text: `${currentPage}/${pageCount}`, alignment: 'left' },
                        { text: `${currentPage}`, alignment: 'left' },
                        { text: '', alignment: 'center' },
                        { text: '', alignment: 'right', fontSize: 8 }
                      ]
                    ]
                  }
                }]
              } else {
                return [{
                  margin: [60, 30, 60, 30],
                  layout: {
                    hLineColor: i => (i === 0) ? 'lightgray' : '',
                    vLineWidth: i => 0,
                    hLineWidth: i => (i === 0) ? 1 : 0
                  },
                  table: {
                    widths: ['*', 160, 160],
                    body: [
                      [
                        { text: ' ' },
                        { text: ' ' },
                        { text: ' ' }
                      ],
                      [
                        { text: book.data.title, alignment: 'left', fontSize: 8 },
                        { text: '#', alignment: 'center' },
                        // { text: `${currentPage}/${pageCount}`, alignment: 'right' }
                        { text: `${currentPage}`, alignment: 'right' }
                      ]
                    ]
                  }
                }]
              }
            }
          }
          // pageBreakBefore (currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
          //   console.log(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage)
          //   // // check if signature part is completely on the last page, add pagebreak if not
          //   // if (currentNode.id === 'signature' && (currentNode.pageNumbers.length != 1 || currentNode.pageNumbers[0] != currentNode.pages)) {
          //   //   return true
          //   // // check if last paragraph is entirely on a single page, add pagebreak if not
          //   // } else if (currentNode.id === 'closingParagraph' && currentNode.pageNumbers.length != 1) {
          //   //   return true
          //   // }

          //   // return false
          // }
        }

        pdfMake.createPdf(docDefinition).open()
      })
    })
  },
  pdfDownloader2_bup (book) {
    const docHtml = []
    const parser = new DOMParser()
    fetch('/pdf_skeleton.html')
      .then(r => r.text())
      .then((text) => {
        docHtml.doc = parser.parseFromString(text, 'text/html')

        // if (book.data.abstract.length) {
        //   const node = docHtml.doc.createElement('h4')
        //   node.className = 'abstract'
        //   node.innerHTML = book.data.abstract
        //   docHtml.doc.getElementById('main').appendChild(node)
        // }
        const main = docHtml.doc.getElementById('main')

        const bookCover = docHtml.doc.createElement('div')
        bookCover.innerHTML = '<h1>' + book.data.title + '</h1'
        main.appendChild(bookCover)

        const bookCoverRetro = docHtml.doc.createElement('div')
        bookCoverRetro.innerHTML = '<h1>&nbsp;</h1'
        bookCoverRetro.className = 'pdf-pagebreak-before'
        main.appendChild(bookCoverRetro)

        book.data.chapters.forEach((chapter, i) => {
          const node = docHtml.doc.createElement('h2')
          node.className = 'title'
          node.innerHTML = chapter.title
          node.className += ' pdf-pagebreak-before'
          main.appendChild(node)

          chapter.content.blocks.forEach((block, index) => {
            const node = docHtml.doc.createElement('P')

            node.className = 'blocks'

            if (block.type === 'paragraph') {
              node.innerHTML = block.data.text
              docHtml.doc.getElementById('main').appendChild(node)
            }

            if (block.type === 'image') {
              const imagenode = docHtml.doc.createElement('IMG')
              imagenode.src = block.data.url
              node.appendChild(imagenode)
              docHtml.doc.getElementById('main').appendChild(node)
            }

            // chapter.children.forEach((child) => {
            //   const node = docHtml.doc.createElement('h3')
            //   node.className = 'title'
            //   node.innerHTML = child.title
            //   docHtml.doc.getElementById('main').appendChild(node)

            //   child.content.blocks.forEach((block, index) => {
            //     if (block.type === 'paragraph') {
            //       node.innerHTML = block.data.text
            //       docHtml.doc.getElementById('main').appendChild(node)
            //     }
            //     if (block.type === 'image') {
            //       const imagenode = docHtml.doc.createElement('IMG')
            //       imagenode.src = block.data.url
            //       node.appendChild(imagenode)
            //       docHtml.doc.getElementById('main').appendChild(node)
            //     }
            //   })
            // })
          })
        })

        const html = htmlToPdfMake('<html>' + docHtml.doc.documentElement.innerHTML + '</html>')

        const docDefinition = {
          content: [
            html
          ],
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [60, 80, 60, 120],
          footer (currentPage, pageCount) {
            if (currentPage > 2) {
              if (currentPage % 2 === 0) {
                return [{
                  margin: [60, 30, 60, 30],
                  layout: {
                    hLineColor: i => (i === 0) ? 'lightgray' : '',
                    vLineWidth: i => 0,
                    hLineWidth: i => (i === 0) ? 1 : 0
                  },
                  table: {
                    widths: ['*', 160, 160],
                    body: [
                      [
                        { text: ' ' },
                        { text: ' ' },
                        { text: ' ' }
                      ],
                      [
                        // { text: `${currentPage}/${pageCount}`, alignment: 'left' },
                        { text: `${currentPage}`, alignment: 'left' },
                        { text: '', alignment: 'center' },
                        { text: '', alignment: 'right', fontSize: 8 }
                      ]
                    ]
                  }
                }]
              } else {
                return [{
                  margin: [60, 30, 60, 30],
                  layout: {
                    hLineColor: i => (i === 0) ? 'lightgray' : '',
                    vLineWidth: i => 0,
                    hLineWidth: i => (i === 0) ? 1 : 0
                  },
                  table: {
                    widths: ['*', 160, 160],
                    body: [
                      [
                        { text: ' ' },
                        { text: ' ' },
                        { text: ' ' }
                      ],
                      [
                        { text: book.data.title, alignment: 'left', fontSize: 8 },
                        { text: '#', alignment: 'center' },
                        // { text: `${currentPage}/${pageCount}`, alignment: 'right' }
                        { text: `${currentPage}`, alignment: 'right' }
                      ]
                    ]
                  }
                }]
              }
            }
          },
          pageBreakBefore (currentNode) {
            return currentNode.style && currentNode.style.includes('pdf-pagebreak-before')
          }
        }

        pdfMake.createPdf(docDefinition).open()
      })
  }
}

// export default utils

// eslint-disable-next-line no-unused-vars
// const docDefinition = {
//   // info: {
//   //   title: book.data.title,
//   //   author: 'john doe',
//   //   subject: book.data.abstract,
//   //   keywords: 'keywords for document'
//   // },
//   // content: pdfContent,
//   pageSize: 'A4',
//   pageOrientation: 'portrait',
//   pageMargins: [60, 80, 60, 120]
//   // footer (currentPage, pageCount) {
//   //   if (currentPage > 3) {
//   //     if (currentPage % 2 === 0) {
//   //       return [{
//   //         margin: [60, 30, 60, 30],
//   //         layout: {
//   //           hLineColor: i => (i === 0) ? 'lightgray' : '',
//   //           vLineWidth: i => 0,
//   //           hLineWidth: i => (i === 0) ? 1 : 0
//   //         },
//   //         table: {
//   //           widths: ['*', 160, 160],
//   //           body: [
//   //             [
//   //               { text: ' ' },
//   //               { text: ' ' },
//   //               { text: ' ' }
//   //             ],
//   //             [
//   //               // { text: `${currentPage}/${pageCount}`, alignment: 'left' },
//   //               { text: `${currentPage}`, alignment: 'left' },
//   //               { text: '', alignment: 'center' },
//   //               { text: '', alignment: 'right', fontSize: 8 }
//   //             ]
//   //           ]
//   //         }
//   //       }]
//   //     } else {
//   //       return [{
//   //         margin: [60, 30, 60, 30],
//   //         layout: {
//   //           hLineColor: i => (i === 0) ? 'lightgray' : '',
//   //           vLineWidth: i => 0,
//   //           hLineWidth: i => (i === 0) ? 1 : 0
//   //         },
//   //         table: {
//   //           widths: ['*', 160, 160],
//   //           body: [
//   //             [
//   //               { text: ' ' },
//   //               { text: ' ' },
//   //               { text: ' ' }
//   //             ],
//   //             [
//   //               { text: book.data.title, alignment: 'left', fontSize: 8 },
//   //               { text: '#', alignment: 'center' },
//   //               // { text: `${currentPage}/${pageCount}`, alignment: 'right' }
//   //               { text: `${currentPage}`, alignment: 'right' }
//   //             ]
//   //           ]
//   //         }
//   //       }]
//   //     }
//   //   }
//   // }
//   // pageBreakBefore (currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
//   //   console.log(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage)
//   //   // // check if signature part is completely on the last page, add pagebreak if not
//   //   // if (currentNode.id === 'signature' && (currentNode.pageNumbers.length != 1 || currentNode.pageNumbers[0] != currentNode.pages)) {
//   //   //   return true
//   //   // // check if last paragraph is entirely on a single page, add pagebreak if not
//   //   // } else if (currentNode.id === 'closingParagraph' && currentNode.pageNumbers.length != 1) {
//   //   //   return true
//   //   // }

//   //   // return false
//   // }
// }

export default ({ store, app }, inject) => {
  inject('pdfUtils', (book) => {
    console.log(app.state)
  })
}
